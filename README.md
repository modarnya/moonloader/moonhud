# Build

Use [https://github.com/LuaDist/squish](https://github.com/LuaDist/squish)

# Requirements

Moon ImGui (old Moonloader ImGui)


MoonAdditions [https://github.com/THE-FYP/MoonAdditions](https://github.com/THE-FYP/MoonAdditions)


SAMP Events [https://github.com/THE-FYP/SAMP.Lua](https://github.com/THE-FYP/SAMP.Lua)

# Config

See config examples in `config` directory