local NATIVE_RESOLUTION_X, NATIVE_RESOLUTION_Y = 640, 448

function select_position_and(do_what)
      lua_thread.create(function()
            window_state.v = false

            while true do
                  wait(0)

                  sampSetCursorMode(3)

                  do_what()

                  if isKeyJustPressed(1) then
                        sampSetCursorMode(0)
                        window_state.v = true
                        return
                  end
            end
      end)
end

function select_position_for(x_ptr, y_ptr)
      select_position_and(function()
            x_ptr.v, y_ptr.v = getCursorPos()
      end)
end

function imgui.SimpleTextureConfiguration(label, config)
      imgui.Checkbox("Display icon" .. "##" .. label, config.icon)
      imgui.InputInt(fa.ICON_SLIDERS_H .. "  Icon position x" .. "##" .. label, config.icon_pos_x)
      imgui.InputInt(fa.ICON_SLIDERS_H .. "  Icon position y" .. "##" .. label, config.icon_pos_y)
      if imgui.Button(fa.ICON_RETWEET .. "  Choose icon position" .. "##" .. label, imgui.ImVec2(225, 20)) then
            select_position_for(config.icon_pos_x, config.icon_pos_y)
      end
      imgui.SliderInt(fa.ICON_SLIDERS_H .. "  Icon size" .. "##" .. label, config.icon_size, 0, 200)
      imgui.ColorEdit4(fa.ICON_SPINNER .. "  Icon color" .. "##" .. label, config.icon_color)
end

function imgui.SimpleBorderBoxConfiguration(label, config)
      imgui.Checkbox("Display bar" .. "##" .. label, config.bar)
      imgui.InputInt(fa.ICON_SLIDERS_H .. "  Bar position x" .. "##" .. label, config.bar_pos_x)
      imgui.InputInt(fa.ICON_SLIDERS_H .. "  Bar position y" .. "##" .. label, config.bar_pos_y)
      if imgui.Button(fa.ICON_RETWEET .. "  Choose bar position" .. "##" .. label, imgui.ImVec2(225, 20)) then
            select_position_for(config.bar_pos_x, config.bar_pos_y)
      end
      imgui.SliderInt(fa.ICON_SLIDERS_H .. "  Bar width" .. "##" .. label, config.bar_size_x, 0, 400)
      imgui.SliderInt(fa.ICON_SLIDERS_H .. "  Bar height" .. "##" .. label, config.bar_size_y, 0, 200)
      imgui.SliderInt(fa.ICON_SLIDERS_H .. "  Border size" .. "##" .. label, config.border_size, 0, 20)

      imgui.ColorEdit4(fa.ICON_SPINNER .. "  Bar fill color" .. "##" .. label, config.main_color)
      imgui.ColorEdit4(fa.ICON_SPINNER .. "  Bar background color" .. "##" .. label, config.background_color)
      imgui.ColorEdit4(fa.ICON_SPINNER .. "  Bar border color" .. "##" .. label, config.border_color)
end

function imgui.SimpleTextdrawConfiguration(label, config)
      imgui.InputInt(fa.ICON_SLIDERS_H .. "  Text position x" .. "##" .. label, config.pos_x)
      imgui.InputInt(fa.ICON_SLIDERS_H .. "  Text position y" .. "##" .. label, config.pos_y)
      if imgui.Button(fa.ICON_RETWEET .. "  Choose text position" .. "##" .. label, imgui.ImVec2(225, 20)) then
            select_position_for(config.pos_x, config.pos_y)
      end
      imgui.SliderFloat(fa.ICON_SLIDERS_H .. "  Text width" .. "##" .. label, config.scale_x, 0, 20)
      imgui.SliderFloat(fa.ICON_SLIDERS_H .. "  Text height" .. "##" .. label, config.scale_y, 0, 20)
      imgui.ColorEdit4(fa.ICON_SPINNER .. "  Text color" .. "##" .. label, config.text_color)
      imgui.ColorEdit4(fa.ICON_SPINNER .. "  Shadow color" .. "##" .. label, config.shadow_color)
      imgui.Text("Style:")
      imgui.RadioButton("SUBTITLES" .. "##" .. label, config.style, 1)
      imgui.SameLine()
      imgui.RadioButton("MENU" .. "##" .. label, config.style, 2)
      imgui.SameLine()
      imgui.RadioButton("GOTHIC" .. "##" .. label, config.style, 3)
      imgui.SameLine()
      imgui.RadioButton("PRICEDOWN" .. "##" .. label, config.style, 4)

      imgui.Text("Alignment:")
      imgui.RadioButton("Left" .. "##" .. label, config.align, 1)
      imgui.SameLine()
      imgui.RadioButton("Center" .. "##" .. label, config.align, 2)
      imgui.SameLine()
      imgui.RadioButton("Right" .. "##" .. label, config.align, 3)
end

local fa_font = nil

function imgui.BeforeDrawFrame()
      if fa_font then
            return
      end

      local fa_glyph_ranges = imgui.ImGlyphRanges({
            fa.min_range,
            fa.max_range,
      })

      local font_config = imgui.ImFontConfig()
      font_config.MergeMode = true
      font_config.SizePixels = 15.0
      font_config.GlyphExtraSpacing.x = 0.1
      fa_font = imgui.GetIO().Fonts:AddFontFromMemoryCompressedBase85TTF(fa.get_font_data_base85(), font_config.SizePixels, font_config, fa_glyph_ranges)
end

function imgui.OnDrawFrame()
      if not window_state.v then
            return
      end

      imgui.SetNextWindowPos(imgui.ImVec2(imgui.GetIO().DisplaySize.x / 4, imgui.GetIO().DisplaySize.y / 2), imgui.Cond.FirstUseEver, imgui.ImVec2(0.5, 0.5))
      imgui.SetNextWindowSize(imgui.ImVec2(1000, 600), imgui.Cond.FirstUseEver)
      imgui.Begin(fa.ICON_COGS .. " MoonHUD | Author: THERION", window_state, imgui.WindowFlags.AlwaysAutoResize)

      imgui.BeginChild(1, imgui.ImVec2(814, 84), true)

      imgui.SetCursorPos(imgui.ImVec2(5, 5))

      if imgui.OnOffSwitch(game.config.GENERAL.on) then
            game.config.GENERAL.on = not game.config.GENERAL.on
            toggle_hud(game.config)
            add_chat_message("script is now " .. (game.config.GENERAL.on and "{5BFF83}ON" or "{FB4343}OFF"))
      end

      imgui.SameLine()
      imgui.SetCursorPosX(167)

      if imgui.BigControlButton(fa.ICON_SAVE .. "  Save") then
            game.save_config()
            server.save_config()
            add_chat_message("configuration is saved")
      end

      imgui.SameLine()
      imgui.SetCursorPosX(329)

      if imgui.BigControlButton(fa.ICON_REDO_ALT .. "  Default") then
            game.reset_config()
            server.reset_config()
            add_chat_message("configuration is reset")
      end

      imgui.SameLine()
      imgui.SetCursorPosX(491)

      if imgui.BigControlButton("Click me") then
            os.execute("explorer \"https://youtu.be/dQw4w9WgXcQ\"")
      end

      imgui.SameLine()
      imgui.SetCursorPosX(653)

      if imgui.MenuOption(fa.ICON_LIST .. "  Kill-List", selected == 0, imgui.ImVec2(161, 75)) then
            selected = 0
      end

      imgui.EndChild()

      imgui.BeginChild(2, imgui.ImVec2(814, 439), true)
      imgui.FeaturesConfiguration()
      imgui.ServersFeaturesConfiguration()
      imgui.EndChild()


      imgui.BeginChild(3, imgui.ImVec2(814, 84), true)
      imgui.FeatureSelectBox()
      imgui.EndChild()

      if is_galaxy then
            imgui.SetCursorPos(imgui.ImVec2(824, 28))

            imgui.BeginChild(4, imgui.ImVec2(109, 615), true)
            imgui.ServersFeatureSelectBox()
            imgui.EndChild()
      end

      imgui.End()
end