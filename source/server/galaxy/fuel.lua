local module = {}

module.fuel_td = nil
module.fuel_string = ""

-- could be refactored to something proper if time was infinite
local FUEL_TEXTDRAW_PREFIX = utf8:decode("~p~ToЈћњўo: ")
local FUEL_TEXTDRAW_VALUE_EXTRACT_PATTERN = utf8:decode("~(%d+)")
local FUEL_TEXTDRAW_NEW_FORMAT = utf8:decode("ToЈћњўo: ~%s~%d%%")

local function get_fuel_textdraw_color(fuel_percent)
      if fuel_percent < 10 then
            return "r"
      end

      if fuel_percent < 25 then
            return "y"
      end

      return "g"
end

local function on_show_textdraw(id, textdraw)
      if not textdraw.text:match(FUEL_TEXTDRAW_PREFIX) then
            return
      end

      module.fuel_td = id

      local fuel =  tonumber(textdraw.text:match(FUEL_TEXTDRAW_VALUE_EXTRACT_PATTERN))
      local fuel_color = get_fuel_textdraw_color(fuel)

      module.fuel_string = FUEL_TEXTDRAW_NEW_FORMAT:format(fuel_color, fuel)

      return false
end

local function on_textdraw_set_string(id, text)
      if not text:match(FUEL_TEXTDRAW_PREFIX) then
            return
      end

      module.fuel_td = id

      local fuel =  tonumber(text:match(FUEL_TEXTDRAW_VALUE_EXTRACT_PATTERN))
      local fuel_color = get_fuel_textdraw_color(fuel)

      module.fuel_string = FUEL_TEXTDRAW_NEW_FORMAT:format(fuel_color, fuel)
end

local function on_textdraw_hide(id)
      if id == module.fuel_td then
            module.fuel_td = nil
      end
end

function module.init_hooks()
      samp_events.onShowTextDraw = on_show_textdraw
      samp_events.onTextDrawSetString = on_textdraw_set_string
      samp_events.onTextDrawHide = on_textdraw_hide
end

function module.render(config)
      if not isCharInAnyCar(PLAYER_PED) then
            return
      end


      if not config.FUEL.on.v then
            return
      end

      if not module.fuel_td then
            return
      end

      render_simple_textdraw(config.FUEL, module.fuel_string)
end

return module