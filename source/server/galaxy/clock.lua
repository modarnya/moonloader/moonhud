local module = {}

local CLOCK_TEXTDRAW_STATIC_ID = 0
local CLOCK_TEXTDRAW_PATTERN = "~w~%d+~y~:~w~%d+"
local CLOCK_TEXTDRAW_HOURS_AND_MINUTES_EXTRACTOR = "~w~(%d+)~y~:~w~(%d+)"
local CLOCK_TEXTDRAW_NEW_FORMAT = "%02d:%02d"

function module.render(config)
      if not sampTextdrawIsExists(CLOCK_TEXTDRAW_STATIC_ID) then
            return
      end

      local textdraw_string = sampTextdrawGetString(CLOCK_TEXTDRAW_STATIC_ID)

      if textdraw_string:match(CLOCK_TEXTDRAW_PATTERN) then
            hide_textdraw(CLOCK_TEXTDRAW_STATIC_ID)
      end

      if not config.CLOCK.on.v then
            return
      end

      local server_hours, server_minutes = textdraw_string:match(CLOCK_TEXTDRAW_HOURS_AND_MINUTES_EXTRACTOR)
      local text = CLOCK_TEXTDRAW_NEW_FORMAT:format(server_hours, server_minutes)
      render_simple_textdraw(config.CLOCK, text)
end

return module