local module = {}

local LEVEL_TEXTDRAW_STATIC_ID = 2048
local LEVEL_TEXTDRAW_PATTERN = "~w~LvL: ~y~%d+ ~w~Exp: ~y~%d+~w~/~y~%d+"

function module.render(config)
      if not sampTextdrawIsExists(LEVEL_TEXTDRAW_STATIC_ID) then
            return
      end

      local textdraw_string = sampTextdrawGetString(LEVEL_TEXTDRAW_STATIC_ID)

      if textdraw_string:match(LEVEL_TEXTDRAW_PATTERN) then
            hide_textdraw(LEVEL_TEXTDRAW_STATIC_ID)
      end

      if not config.LEVEL.on.v then
            return
      end

      local text = sampTextdrawGetString(LEVEL_TEXTDRAW_STATIC_ID)
      render_simple_textdraw(config.LEVEL, text)
end

return module