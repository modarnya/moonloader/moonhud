local GALAXY_FEATURES = {
      fa.ICON_PIZZA_SLICE .. "  Food",
      fa.ICON_MEDKIT .. "  Meds",
      fa.ICON_PILLS .. "  Drugs",
      fa.ICON_CALENDAR_TIMES .. "  Date",
      fa.ICON_CLOCK .. "  Time",
      fa.ICON_GAS_PUMP .. "  Fuel",
      fa.ICON_LEVEL_UP_ALT .. "  Level"
}

function imgui.GalaxyFeaturesConfiguration(config)
      if selected == 9 then
            imgui.Checkbox("Hide when no food", config.FOOD.zero_disable)
            imgui.SimpleTextureConfiguration("galaxy_food", config.FOOD)
            imgui.SimpleBorderBoxConfiguration("galaxy_food", config.FOOD)
            imgui.SimpleTextdrawConfiguration("galaxy_food", config.FOOD)
      end
      if selected == 10 then
            imgui.Checkbox("Hide when no pills", config.MEDS.zero_disable)
            imgui.SimpleTextureConfiguration("galaxy_meds", config.MEDS)
            imgui.SimpleBorderBoxConfiguration("galaxy_meds", config.MEDS)
            imgui.SimpleTextdrawConfiguration("galaxy_meds", config.MEDS)
      end
      if selected == 11 then
            imgui.Checkbox("Enabled##galaxy_drugs", config.DRUGS.on)
            imgui.SimpleTextureConfiguration("galaxy_drugs", config.DRUGS)
            imgui.ColorEdit4(fa.ICON_SPINNER .. " No cooldown color", config.DRUGS.icon_color_zero)
            imgui.SimpleBorderBoxConfiguration("galaxy_drugs", config.DRUGS)
            -- imgui.ColorEdit4(fa.ICON_SPINNER .. " No cooldown bar color", config.DRUGS.zero_color)
            imgui.SimpleTextdrawConfiguration("galaxy_drugs", config.DRUGS)
            imgui.Checkbox("Display \'-\' when no cooldown", config.DRUGS.display_word)
      end
      if selected == 12 then
            imgui.Checkbox("Enabled##galaxy_date", config.DATE.on)
            imgui.Checkbox("One line", config.DATE.oneline)
            imgui.SimpleTextdrawConfiguration("galaxy_date", config.DATE)
      end
      if selected == 13 then
            imgui.Checkbox("Enabled##galaxy_clock", config.CLOCK.on)
            imgui.SimpleTextdrawConfiguration("galaxy_clock", config.CLOCK)
      end
      if selected == 14 then
            imgui.Checkbox("Enabled##galaxy_fuel", config.FUEL.on)
            imgui.SimpleTextdrawConfiguration("galaxy_fuel", config.FUEL)
      end
      if selected == 15 then
            imgui.Checkbox("Enabled##galaxy_level", config.LEVEL.on)
            imgui.SimpleTextdrawConfiguration("galaxy_level", config.LEVEL)
      end
end

function imgui.GalaxyFeatureSelectBox()
      return imgui.DrawSelectBox(GALAXY_FEATURES, 100, 75, 1, false, 8)
end