local module = {}

local MEDS_TEXTDRAWS_LOWER_BOUND = 2049
local MEDS_TEXTDRAWS_UPPER_BOUND = 2069

local MEDS_ICON_MODEL = 1240
local MEDS_ICON_ROTATION = { x = 0, y = 0, z = 0 }
local MEDS_ICON_ZOOM = 1

local MEDS_LIMIT = 5

local function is_textdraw_meds_icon(id)
      local model, rot_x, rot_y, rot_z, zoom = sampTextdrawGetModelRotationZoomVehColor(id)

      if model ~= MEDS_ICON_MODEL then
            return false
      end

      if rot_x ~= MEDS_ICON_ROTATION.x or rot_y ~= MEDS_ICON_ROTATION.y or rot_z ~= MEDS_ICON_ROTATION.z then
            return false
      end

      if zoom ~= MEDS_ICON_ZOOM then
            return false
      end

      return true
end

local function do_meds_textdraws_exist(icon, value)
      if not icon then
            return false
      end

      if not value then
            return false
      end

      if not sampTextdrawIsExists(icon) then
            return false
      end

      if not sampTextdrawIsExists(value) then
            return false
      end

      return true
end

function module.init_resources()
      textures.meds = moon_additions.load_png_texture(RESOURCE_PATH .. "/pill.png")
end

function module.render(config)
      local meds_icon_textdraw, meds_value_textdraw

      for i = MEDS_TEXTDRAWS_LOWER_BOUND, MEDS_TEXTDRAWS_UPPER_BOUND do
	  	if not sampTextdrawIsExists(i) then
		  	goto continue
	  	end

            if not is_textdraw_meds_icon(i) then
                  goto continue
            end

            meds_icon_textdraw = i
            meds_value_textdraw = i + 1

            ::continue::
  	end


      if not do_meds_textdraws_exist(meds_icon_textdraw, meds_value_textdraw) and config.MEDS.zero_disable.v then
            return
      end

      if not do_meds_textdraws_exist(meds_icon_textdraw, meds_value_textdraw) and not config.MEDS.zero_disable.v then
            if config.MEDS.icon.v then
                  render_simple_texture(config.MEDS, textures.meds)
            end

            if config.MEDS.bar.v then
                  render_simple_borderbox(config.MEDS, 0)
            end

            if config.MEDS.text.v then
                  render_simple_textdraw(config.MEDS, "0")
            end

            return
      end

      local meds_value = tonumber(sampTextdrawGetString(meds_value_textdraw)) or 0

      hide_textdraw(meds_icon_textdraw)
      hide_textdraw(meds_value_textdraw)

      if config.MEDS.icon.v then
            render_simple_texture(config.MEDS, textures.meds)
      end

      if config.MEDS.bar.v then
            render_simple_borderbox(config.MEDS, meds_value / MEDS_LIMIT)
      end

      if config.MEDS.text.v then
            render_simple_textdraw(config.MEDS, tostring(meds_value))
      end
end

return module