local module = {}

module.config_path = "moonhud/galaxy.ini"
module.ip = {
      ["194.147.32.37"] = 1,
}
module.get_config = require("server.galaxy.config")
module.serialization = require("server.galaxy.serialization")
module.config = nil

local clock = require("server.galaxy.clock")
local date = require("server.galaxy.date")
local drugs = require("server.galaxy.drugs")
local food = require("server.galaxy.food")
local meds = require("server.galaxy.meds")
local fuel = require("server.galaxy.fuel")
local level = require("server.galaxy.level")

require("server.galaxy.menu")

function module.load_config()
      module.config = inicfg.load(module.get_config(), module.config_path)

      if doesFileExist(module.config_path) then
            inicfg.save(module.config)
      end

      module.config = module.serialization.to(module.config)
end

function module.save_config()
      local success = false

      module.config = module.serialization.from(module.config)
      success = inicfg.save(module.config, module.config_path)
      module.config = module.serialization.to(module.config)

      return success
end

function module.reset_config()
      module.config = module.get_config()
      module.config = module.serialization.to(module.config)
end

function module.init()
      module.load_config()

      food.init_resources()
      meds.init_resources()
      drugs.init_resources()

      drugs.init_hooks()
      fuel.init_hooks()
end

function module.render()
      food.render(module.config)
      meds.render(module.config)
      drugs.render(module.config)
      clock.render(module.config)
      date.render(module.config)
      level.render(module.config)
      fuel.render(module.config)
end

return module