local module = {}

function module.to(config)
      config.FOOD.zero_disable = imgui.ImBool(config.FOOD.zero_disable)
      config.FOOD.icon = imgui.ImBool(config.FOOD.icon)
      config.FOOD.icon_pos_x = imgui.ImInt(config.FOOD.icon_pos_x)
      config.FOOD.icon_pos_y = imgui.ImInt(config.FOOD.icon_pos_y)
      config.FOOD.icon_size = imgui.ImInt(config.FOOD.icon_size)
      a, r, g, b = hex_to_argb(config.FOOD.icon_color)
      config.FOOD.icon_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())

      config.FOOD.bar = imgui.ImBool(config.FOOD.bar)
      config.FOOD.bar_pos_x = imgui.ImInt(config.FOOD.bar_pos_x)
      config.FOOD.bar_pos_y = imgui.ImInt(config.FOOD.bar_pos_y)
      config.FOOD.bar_size_x = imgui.ImInt(config.FOOD.bar_size_x)
      config.FOOD.bar_size_y = imgui.ImInt(config.FOOD.bar_size_y)
      config.FOOD.border_size = imgui.ImInt(config.FOOD.border_size)
      a, r, g, b = hex_to_argb(config.FOOD.main_color)
      config.FOOD.main_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.FOOD.border_color)
      config.FOOD.border_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.FOOD.background_color)
      config.FOOD.background_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())

      config.FOOD.text = imgui.ImBool(config.FOOD.text)
      config.FOOD.pos_x = imgui.ImInt(config.FOOD.pos_x)
      config.FOOD.pos_y = imgui.ImInt(config.FOOD.pos_y)
      config.FOOD.style = imgui.ImInt(config.FOOD.style)
      config.FOOD.scale_x = imgui.ImFloat(config.FOOD.scale_x)
      config.FOOD.scale_y = imgui.ImFloat(config.FOOD.scale_y)
      config.FOOD.align = imgui.ImInt(config.FOOD.align)
      a, r, g, b = hex_to_argb(config.FOOD.text_color)
      config.FOOD.text_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.FOOD.shadow_color)
      config.FOOD.shadow_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())

      config.MEDS.zero_disable = imgui.ImBool(config.MEDS.zero_disable)
      config.MEDS.icon = imgui.ImBool(config.MEDS.icon)
      config.MEDS.icon_pos_x = imgui.ImInt(config.MEDS.icon_pos_x)
      config.MEDS.icon_pos_y = imgui.ImInt(config.MEDS.icon_pos_y)
      config.MEDS.icon_size = imgui.ImInt(config.MEDS.icon_size)
      a, r, g, b = hex_to_argb(config.MEDS.icon_color)
      config.MEDS.icon_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())

      config.MEDS.bar = imgui.ImBool(config.MEDS.bar)
      config.MEDS.bar_pos_x = imgui.ImInt(config.MEDS.bar_pos_x)
      config.MEDS.bar_pos_y = imgui.ImInt(config.MEDS.bar_pos_y)
      config.MEDS.bar_size_x = imgui.ImInt(config.MEDS.bar_size_x)
      config.MEDS.bar_size_y = imgui.ImInt(config.MEDS.bar_size_y)
      config.MEDS.border_size = imgui.ImInt(config.MEDS.border_size)
      a, r, g, b = hex_to_argb(config.MEDS.main_color)
      config.MEDS.main_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.MEDS.border_color)
      config.MEDS.border_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.MEDS.background_color)
      config.MEDS.background_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())

      config.MEDS.text = imgui.ImBool(config.MEDS.text)
      config.MEDS.pos_x = imgui.ImInt(config.MEDS.pos_x)
      config.MEDS.pos_y = imgui.ImInt(config.MEDS.pos_y)
      config.MEDS.style = imgui.ImInt(config.MEDS.style)
      config.MEDS.scale_x = imgui.ImFloat(config.MEDS.scale_x)
      config.MEDS.scale_y = imgui.ImFloat(config.MEDS.scale_y)
      config.MEDS.align = imgui.ImInt(config.MEDS.align)
      a, r, g, b = hex_to_argb(config.MEDS.text_color)
      config.MEDS.text_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.MEDS.shadow_color)
      config.MEDS.shadow_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())

      config.DRUGS.on = imgui.ImBool(config.DRUGS.on)
      config.DRUGS.icon = imgui.ImBool(config.DRUGS.icon)
      config.DRUGS.icon_pos_x = imgui.ImInt(config.DRUGS.icon_pos_x)
      config.DRUGS.icon_pos_y = imgui.ImInt(config.DRUGS.icon_pos_y)
      config.DRUGS.icon_size = imgui.ImInt(config.DRUGS.icon_size)
      a, r, g, b = hex_to_argb(config.DRUGS.icon_color)
      config.DRUGS.icon_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.DRUGS.icon_color_zero)
      config.DRUGS.icon_color_zero = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())

      config.DRUGS.bar = imgui.ImBool(config.DRUGS.bar)
      config.DRUGS.bar_pos_x = imgui.ImInt(config.DRUGS.bar_pos_x)
      config.DRUGS.bar_pos_y = imgui.ImInt(config.DRUGS.bar_pos_y)
      config.DRUGS.bar_size_x = imgui.ImInt(config.DRUGS.bar_size_x)
      config.DRUGS.bar_size_y = imgui.ImInt(config.DRUGS.bar_size_y)
      config.DRUGS.border_size = imgui.ImInt(config.DRUGS.border_size)
      a, r, g, b = hex_to_argb(config.DRUGS.main_color)
      config.DRUGS.main_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.DRUGS.zero_color)
      config.DRUGS.zero_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.DRUGS.border_color)
      config.DRUGS.border_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.DRUGS.background_color)
      config.DRUGS.background_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())

      config.DRUGS.text = imgui.ImBool(config.DRUGS.text)
      config.DRUGS.display_word = imgui.ImBool(config.DRUGS.display_word)
      config.DRUGS.pos_x = imgui.ImInt(config.DRUGS.pos_x)
      config.DRUGS.pos_y = imgui.ImInt(config.DRUGS.pos_y)
      config.DRUGS.style = imgui.ImInt(config.DRUGS.style)
      config.DRUGS.scale_x = imgui.ImFloat(config.DRUGS.scale_x)
      config.DRUGS.scale_y = imgui.ImFloat(config.DRUGS.scale_y)
      config.DRUGS.align = imgui.ImInt(config.DRUGS.align)
      a, r, g, b = hex_to_argb(config.DRUGS.text_color)
      config.DRUGS.text_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.DRUGS.shadow_color)
      config.DRUGS.shadow_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())

      config.DATE.on = imgui.ImBool(config.DATE.on)
      config.DATE.oneline = imgui.ImBool(config.DATE.oneline)
      config.DATE.pos_x = imgui.ImInt(config.DATE.pos_x)
      config.DATE.pos_y = imgui.ImInt(config.DATE.pos_y)
      config.DATE.style = imgui.ImInt(config.DATE.style)
      config.DATE.scale_x = imgui.ImFloat(config.DATE.scale_x)
      config.DATE.scale_y = imgui.ImFloat(config.DATE.scale_y)
      config.DATE.align = imgui.ImInt(config.DATE.align)
      a, r, g, b = hex_to_argb(config.DATE.text_color)
      config.DATE.text_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.DATE.shadow_color)
      config.DATE.shadow_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())

      config.CLOCK.on = imgui.ImBool(config.CLOCK.on)
      config.CLOCK.pos_x = imgui.ImInt(config.CLOCK.pos_x)
      config.CLOCK.pos_y = imgui.ImInt(config.CLOCK.pos_y)
      config.CLOCK.style = imgui.ImInt(config.CLOCK.style)
      config.CLOCK.scale_x = imgui.ImFloat(config.CLOCK.scale_x)
      config.CLOCK.scale_y = imgui.ImFloat(config.CLOCK.scale_y)
      config.CLOCK.align = imgui.ImInt(config.CLOCK.align)
      a, r, g, b = hex_to_argb(config.CLOCK.text_color)
      config.CLOCK.text_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.CLOCK.shadow_color)
      config.CLOCK.shadow_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())

      config.FUEL.on = imgui.ImBool(config.FUEL.on)
      config.FUEL.pos_x = imgui.ImInt(config.FUEL.pos_x)
      config.FUEL.pos_y = imgui.ImInt(config.FUEL.pos_y)
      config.FUEL.style = imgui.ImInt(config.FUEL.style)
      config.FUEL.scale_x = imgui.ImFloat(config.FUEL.scale_x)
      config.FUEL.scale_y = imgui.ImFloat(config.FUEL.scale_y)
      config.FUEL.align = imgui.ImInt(config.FUEL.align)
      a, r, g, b = hex_to_argb(config.FUEL.text_color)
      config.FUEL.text_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.FUEL.shadow_color)
      config.FUEL.shadow_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())

      config.LEVEL.on = imgui.ImBool(config.LEVEL.on)
      config.LEVEL.pos_x = imgui.ImInt(config.LEVEL.pos_x)
      config.LEVEL.pos_y = imgui.ImInt(config.LEVEL.pos_y)
      config.LEVEL.style = imgui.ImInt(config.LEVEL.style)
      config.LEVEL.scale_x = imgui.ImFloat(config.LEVEL.scale_x)
      config.LEVEL.scale_y = imgui.ImFloat(config.LEVEL.scale_y)
      config.LEVEL.align = imgui.ImInt(config.LEVEL.align)
      config.LEVEL.text_alpha = imgui.ImInt(config.LEVEL.text_alpha)
      a, r, g, b = hex_to_argb(config.LEVEL.text_color)
      config.LEVEL.text_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.LEVEL.shadow_color)
      config.LEVEL.shadow_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())

      return config
end

function module.from(config)
      config.FOOD.zero_disable = config.FOOD.zero_disable.v
      config.FOOD.icon = config.FOOD.icon.v
      config.FOOD.icon_pos_x = config.FOOD.icon_pos_x.v
      config.FOOD.icon_pos_y = config.FOOD.icon_pos_y.v
      config.FOOD.icon_size = config.FOOD.icon_size.v
      config.FOOD.icon_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.FOOD.icon_color.v[3], config.FOOD.icon_color.v[2], config.FOOD.icon_color.v[1], config.FOOD.icon_color.v[4]):GetVec4()):GetU32()

      config.FOOD.bar = config.FOOD.bar.v
      config.FOOD.bar_pos_x = config.FOOD.bar_pos_x.v
      config.FOOD.bar_pos_y = config.FOOD.bar_pos_y.v
      config.FOOD.bar_size_x = config.FOOD.bar_size_x.v
      config.FOOD.bar_size_y = config.FOOD.bar_size_y.v
      config.FOOD.border_size = config.FOOD.border_size.v
      config.FOOD.main_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.FOOD.main_color.v[3], config.FOOD.main_color.v[2], config.FOOD.main_color.v[1], config.FOOD.main_color.v[4]):GetVec4()):GetU32()
      config.FOOD.border_color = imgui.ImColor(imgui.ImColor.FromFloat4( config.FOOD.border_color.v[3],  config.FOOD.border_color.v[2],  config.FOOD.border_color.v[1],  config.FOOD.border_color.v[4]):GetVec4()):GetU32()
      config.FOOD.background_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.FOOD.background_color.v[3], config.FOOD.background_color.v[2], config.FOOD.background_color.v[1], config.FOOD.background_color.v[4]):GetVec4()):GetU32()

      config.FOOD.text = config.FOOD.text.v
      config.FOOD.pos_x = config.FOOD.pos_x.v
      config.FOOD.pos_y = config.FOOD.pos_y.v
      config.FOOD.style = config.FOOD.style.v
      config.FOOD.scale_x = config.FOOD.scale_x.v
      config.FOOD.scale_y = config.FOOD.scale_y.v
      config.FOOD.align = config.FOOD.align.v
      config.FOOD.text_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.FOOD.text_color.v[3], config.FOOD.text_color.v[2], config.FOOD.text_color.v[1], config.FOOD.text_color.v[4]):GetVec4()):GetU32()
      config.FOOD.shadow_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.FOOD.shadow_color.v[3], config.FOOD.shadow_color.v[2], config.FOOD.shadow_color.v[1], config.FOOD.shadow_color.v[4]):GetVec4()):GetU32()

      config.MEDS.zero_disable = config.MEDS.zero_disable.v
      config.MEDS.icon = config.MEDS.icon.v
      config.MEDS.icon_pos_x = config.MEDS.icon_pos_x.v
      config.MEDS.icon_pos_y = config.MEDS.icon_pos_y.v
      config.MEDS.icon_size = config.MEDS.icon_size.v
      config.MEDS.icon_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.MEDS.icon_color.v[3], config.MEDS.icon_color.v[2], config.MEDS.icon_color.v[1], config.MEDS.icon_color.v[4]):GetVec4()):GetU32()

      config.MEDS.bar = config.MEDS.bar.v
      config.MEDS.bar_pos_x = config.MEDS.bar_pos_x.v
      config.MEDS.bar_pos_y = config.MEDS.bar_pos_y.v
      config.MEDS.bar_size_x = config.MEDS.bar_size_x.v
      config.MEDS.bar_size_y = config.MEDS.bar_size_y.v
      config.MEDS.border_size = config.MEDS.border_size.v
      config.MEDS.main_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.MEDS.main_color.v[3], config.MEDS.main_color.v[2], config.MEDS.main_color.v[1], config.MEDS.main_color.v[4]):GetVec4()):GetU32()
      config.MEDS.border_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.MEDS.border_color.v[3], config.MEDS.border_color.v[2], config.MEDS.border_color.v[1], config.MEDS.border_color.v[4]):GetVec4()):GetU32()
      config.MEDS.background_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.MEDS.background_color.v[3], config.MEDS.background_color.v[2], config.MEDS.background_color.v[1], config.MEDS.background_color.v[4]):GetVec4()):GetU32()

      config.MEDS.text = config.MEDS.text.v
      config.MEDS.pos_x = config.MEDS.pos_x.v
      config.MEDS.pos_y = config.MEDS.pos_y.v
      config.MEDS.style = config.MEDS.style.v
      config.MEDS.scale_x = config.MEDS.scale_x.v
      config.MEDS.scale_y = config.MEDS.scale_y.v
      config.MEDS.align = config.MEDS.align.v
      config.MEDS.text_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.MEDS.text_color.v[3], config.MEDS.text_color.v[2], config.MEDS.text_color.v[1], config.MEDS.text_color.v[4]):GetVec4()):GetU32()
      config.MEDS.shadow_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.MEDS.shadow_color.v[3], config.MEDS.shadow_color.v[2], config.MEDS.shadow_color.v[1], config.MEDS.shadow_color.v[4]):GetVec4()):GetU32()

      config.DRUGS.on = config.DRUGS.on.v
      config.DRUGS.icon = config.DRUGS.icon.v
      config.DRUGS.icon_pos_x = config.DRUGS.icon_pos_x.v
      config.DRUGS.icon_pos_y = config.DRUGS.icon_pos_y.v
      config.DRUGS.icon_size = config.DRUGS.icon_size.v
      config.DRUGS.icon_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.DRUGS.icon_color.v[3], config.DRUGS.icon_color.v[2], config.DRUGS.icon_color.v[1], config.DRUGS.icon_color.v[4]):GetVec4()):GetU32()
      config.DRUGS.icon_color_zero = imgui.ImColor(imgui.ImColor.FromFloat4(config.DRUGS.icon_color_zero.v[3], config.DRUGS.icon_color_zero.v[2], config.DRUGS.icon_color_zero.v[1], config.DRUGS.icon_color_zero.v[4]):GetVec4()):GetU32()

      config.DRUGS.bar = config.DRUGS.bar.v
      config.DRUGS.bar_pos_x = config.DRUGS.bar_pos_x.v
      config.DRUGS.bar_pos_y = config.DRUGS.bar_pos_y.v
      config.DRUGS.bar_size_x = config.DRUGS.bar_size_x.v
      config.DRUGS.bar_size_y = config.DRUGS.bar_size_y.v
      config.DRUGS.border_size = config.DRUGS.border_size.v
      config.DRUGS.main_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.DRUGS.main_color.v[3], config.DRUGS.main_color.v[2], config.DRUGS.main_color.v[1], config.DRUGS.main_color.v[4]):GetVec4()):GetU32()
      config.DRUGS.zero_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.DRUGS.zero_color.v[3], config.DRUGS.zero_color.v[2], config.DRUGS.zero_color.v[1], config.DRUGS.zero_color.v[4]):GetVec4()):GetU32()
      config.DRUGS.border_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.DRUGS.border_color.v[3], config.DRUGS.border_color.v[2], config.DRUGS.border_color.v[1], config.DRUGS.border_color.v[4]):GetVec4()):GetU32()
      config.DRUGS.background_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.DRUGS.background_color.v[3], config.DRUGS.background_color.v[2], config.DRUGS.background_color.v[1], config.DRUGS.background_color.v[4]):GetVec4()):GetU32()

      config.DRUGS.text = config.DRUGS.text.v
      config.DRUGS.display_word = config.DRUGS.display_word.v
      config.DRUGS.pos_x = config.DRUGS.pos_x.v
      config.DRUGS.pos_y = config.DRUGS.pos_y.v
      config.DRUGS.style = config.DRUGS.style.v
      config.DRUGS.scale_x = config.DRUGS.scale_x.v
      config.DRUGS.scale_y = config.DRUGS.scale_y.v
      config.DRUGS.align = config.DRUGS.align.v
      config.DRUGS.text_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.DRUGS.text_color.v[3], config.DRUGS.text_color.v[2], config.DRUGS.text_color.v[1], config.DRUGS.text_color.v[4]):GetVec4()):GetU32()
      config.DRUGS.shadow_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.DRUGS.shadow_color.v[3], config.DRUGS.shadow_color.v[2], config.DRUGS.shadow_color.v[1], config.DRUGS.shadow_color.v[4]):GetVec4()):GetU32()

      config.DATE.on = config.DATE.on.v
      config.DATE.oneline = config.DATE.oneline.v
      config.DATE.pos_x = config.DATE.pos_x.v
      config.DATE.pos_y = config.DATE.pos_y.v
      config.DATE.style = config.DATE.style.v
      config.DATE.scale_x = config.DATE.scale_x.v
      config.DATE.scale_y = config.DATE.scale_y.v
      config.DATE.align = config.DATE.align.v
      config.DATE.text_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.DATE.text_color.v[3], config.DATE.text_color.v[2], config.DATE.text_color.v[1], config.DATE.text_color.v[4]):GetVec4()):GetU32()
      config.DATE.shadow_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.DATE.shadow_color.v[3], config.DATE.shadow_color.v[2], config.DATE.shadow_color.v[1], config.DATE.shadow_color.v[4]):GetVec4()):GetU32()

      config.CLOCK.on = config.CLOCK.on.v
      config.CLOCK.pos_x = config.CLOCK.pos_x.v
      config.CLOCK.pos_y = config.CLOCK.pos_y.v
      config.CLOCK.style = config.CLOCK.style.v
      config.CLOCK.scale_x = config.CLOCK.scale_x.v
      config.CLOCK.scale_y = config.CLOCK.scale_y.v
      config.CLOCK.align = config.CLOCK.align.v
      config.CLOCK.text_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.CLOCK.text_color.v[3], config.CLOCK.text_color.v[2], config.CLOCK.text_color.v[1], config.CLOCK.text_color.v[4]):GetVec4()):GetU32()
      config.CLOCK.shadow_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.CLOCK.shadow_color.v[3], config.CLOCK.shadow_color.v[2], config.CLOCK.shadow_color.v[1], config.CLOCK.shadow_color.v[4]):GetVec4()):GetU32()

      config.FUEL.on = config.FUEL.on.v
      config.FUEL.pos_x = config.FUEL.pos_x.v
      config.FUEL.pos_y = config.FUEL.pos_y.v
      config.FUEL.style = config.FUEL.style.v
      config.FUEL.scale_x = config.FUEL.scale_x.v
      config.FUEL.scale_y = config.FUEL.scale_y.v
      config.FUEL.align = config.FUEL.align.v
      config.FUEL.text_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.FUEL.text_color.v[3], config.FUEL.text_color.v[2], config.FUEL.text_color.v[1], config.FUEL.text_color.v[4]):GetVec4()):GetU32()
      config.FUEL.shadow_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.FUEL.shadow_color.v[3], config.FUEL.shadow_color.v[2], config.FUEL.shadow_color.v[1], config.FUEL.shadow_color.v[4]):GetVec4()):GetU32()

      config.LEVEL.on = config.LEVEL.on.v
      config.LEVEL.pos_x = config.LEVEL.pos_x.v
      config.LEVEL.pos_y = config.LEVEL.pos_y.v
      config.LEVEL.style = config.LEVEL.style.v
      config.LEVEL.scale_x = config.LEVEL.scale_x.v
      config.LEVEL.scale_y = config.LEVEL.scale_y.v
      config.LEVEL.align = config.LEVEL.align.v
      config.LEVEL.text_alpha = config.LEVEL.text_alpha.v
      config.LEVEL.text_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.LEVEL.text_color.v[3], config.LEVEL.text_color.v[2], config.LEVEL.text_color.v[1], config.LEVEL.text_color.v[4]):GetVec4()):GetU32()
      config.LEVEL.shadow_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.LEVEL.shadow_color.v[3], config.LEVEL.shadow_color.v[2], config.LEVEL.shadow_color.v[1], config.LEVEL.shadow_color.v[4]):GetVec4()):GetU32()

      return config
end

return module