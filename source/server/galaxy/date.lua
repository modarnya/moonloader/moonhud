local module = {}

local DATE_TEXTDRAW_STATIC_ID = 4
local DATE_TEXTDRAW_PATTERN = "(%d+).(%d+).(%d+)"

function module.render(config)
      if not sampTextdrawIsExists(DATE_TEXTDRAW_STATIC_ID) then
            return
      end

      local textdraw_string = sampTextdrawGetString(DATE_TEXTDRAW_STATIC_ID)

      if textdraw_string:match(DATE_TEXTDRAW_PATTERN) then
            hide_textdraw(DATE_TEXTDRAW_STATIC_ID)
      end


      if not config.DATE.on.v then
            return
      end

      -- removin` white color
      local text = textdraw_string:gsub('~w~', '')

      if config.DATE.oneline.v then
            -- removin` newline
            text = text:gsub('~n~', ' ')
      end

      render_simple_textdraw(config.DATE, text)
end

return module