local module = {}

local FOOD_TEXTDRAWS_LOWER_BOUND = 2049
local FOOD_TEXTDRAWS_UPPER_BOUND = 2069
local FOOD_ICON_MODEL = 19094
local FOOD_ICON_ROTATION = { x = 40, y = 90, z = 140 }
local FOOD_ICON_ZOOM = 0.75

local FOOD_LIMIT = 7

local function is_textdraw_food_icon(id)
      local model, rot_x, rot_y, rot_z, zoom = sampTextdrawGetModelRotationZoomVehColor(id)

      if model ~= FOOD_ICON_MODEL then
            return false
      end

      if rot_x ~= FOOD_ICON_ROTATION.x or rot_y ~= FOOD_ICON_ROTATION.y or rot_z ~= FOOD_ICON_ROTATION.z then
            return false
      end

      if zoom ~= FOOD_ICON_ZOOM then
            return false
      end

      return true
end

local function do_food_textdraws_exist(icon, value)
      if not icon then
            return false
      end

      if not value then
            return false
      end

      if not sampTextdrawIsExists(icon) then
            return false
      end

      if not sampTextdrawIsExists(value) then
            return false
      end

      return true
end

function module.init_resources()
      textures.food = moon_additions.load_png_texture(RESOURCE_PATH .. "/food.png")
end

function module.render(config)
      local food_icon_textdraw, food_value_textdraw

      for i = FOOD_TEXTDRAWS_LOWER_BOUND, FOOD_TEXTDRAWS_UPPER_BOUND do
	  	if not sampTextdrawIsExists(i) then
		  	goto continue
	  	end

            if not is_textdraw_food_icon(i) then
                  goto continue
            end

            food_icon_textdraw = i
            food_value_textdraw = i + 1

            ::continue::
  	end


      if not do_food_textdraws_exist(food_icon_textdraw, food_value_textdraw) and config.FOOD.zero_disable.v then
            return
      end

      if not do_food_textdraws_exist(food_icon_textdraw, food_value_textdraw) and not config.FOOD.zero_disable.v then
            if config.FOOD.icon.v then
                  render_simple_texture(config.FOOD, textures.food)
            end

            if config.FOOD.bar.v then
                  render_simple_borderbox(config.FOOD, 0)
            end

            if config.FOOD.text.v then
                  render_simple_textdraw(config.FOOD, "0")
            end

            return
      end

      local food_value = tonumber(sampTextdrawGetString(food_value_textdraw)) or 0

      hide_textdraw(food_icon_textdraw)
      hide_textdraw(food_value_textdraw)

      if config.FOOD.icon.v then
            render_simple_texture(config.FOOD, textures.food)
      end

      if config.FOOD.bar.v then
            render_simple_borderbox(config.FOOD, food_value / FOOD_LIMIT)
      end

      if config.FOOD.text.v then
            render_simple_textdraw(config.FOOD, tostring(food_value))
      end
end

return module