local module = {}

module.drugs_timer = 0

local DRUGS_USAGE_COOLDOWN = 68

local DRUG_USAGE_CHAT_NOTIFICATION_COLOR = -1029514582

local DRUG_USAGE_CHAT_NOTIFICATION_MALE_COCAINE_PATTERN = utf8:decode("(%g+)%sдостал%sпакетик%,%sнасыпал%sбелого%sпорошка%,%sсвернул%sстодолларовую%sкупюру%sв%sтрубочку%sи%sначал%sнюхать%.")
local DRUG_USAGE_CHAT_NOTIFICATION_FEMALE_COCAINE_PATTERN = utf8:decode("(%g+)%sдостала%sпакетик%,%sнасыпала%sбелого%sпорошка%,%sсвернула%sстодолларовую%sкупюру%sв%sтрубочку%sи%sначала%sнюхать%.")
local DRUG_USAGE_CHAT_NOTIFICATION_POT_PATTERN = utf8:decode("(%g+)%sприкуривает%sкосяк%sи%sпыхает%.")

local DRUG_USAGE_PATTERNS = {
      DRUG_USAGE_CHAT_NOTIFICATION_MALE_COCAINE_PATTERN,
      DRUG_USAGE_CHAT_NOTIFICATION_FEMALE_COCAINE_PATTERN,
      DRUG_USAGE_CHAT_NOTIFICATION_POT_PATTERN,
}

local function get_local_player_name()
      local _, id = sampGetPlayerIdByCharHandle(PLAYER_PED)
      return sampGetPlayerNickname(id)
end

local function on_server_message(color, message)
      if color ~= DRUG_USAGE_CHAT_NOTIFICATION_COLOR then
            return
      end

      local player_name = get_local_player_name()

      for i = 1, #DRUG_USAGE_PATTERNS do
            local matched_name = string.match(message, DRUG_USAGE_PATTERNS[i])
            if matched_name == player_name then
                  module.drugs_timer = os.clock() + DRUGS_USAGE_COOLDOWN
            end
      end
end

local function on_send_spawn()
      module.drugs_timer = 0
end

function module.init_resources()
      textures.drugs = moon_additions.load_png_texture(RESOURCE_PATH .. "/drugs.png")
end

function module.init_hooks()
      samp_events.onServerMessage = on_server_message
      samp_events.onSendSpawn = on_send_spawn
end

function module.render(config)
      if not config.DRUGS.on.v then
            return
      end

      local current_cooldown = math.floor(module.drugs_timer - os.clock())

      if config.DRUGS.bar.v then
            local ratio = current_cooldown / DRUGS_USAGE_COOLDOWN
            render_simple_borderbox(config.DRUGS, ratio)
      end

      if config.DRUGS.text.v and current_cooldown > 0 then
            local text = tostring(current_cooldown)
            render_simple_textdraw(config.DRUGS, text)
      end

      if config.DRUGS.text.v and current_cooldown <= 0 and config.DRUGS.display_word.v then
            render_simple_textdraw(config.DRUGS, "-")
      end

      if config.DRUGS.icon.v then
            local left, top, right, bottom, color
            left = config.DRUGS.icon_pos_x.v
            top = config.DRUGS.icon_pos_y.v
            right = config.DRUGS.icon_pos_x.v + config.DRUGS.icon_size.v
            bottom = config.DRUGS.icon_pos_y.v + config.DRUGS.icon_size.v
            color = current_cooldown > 0 and config.DRUGS.icon_color.v or config.DRUGS.icon_color_zero.v

            render_texture(textures.drugs, left, top, right, bottom, color)
      end
end

return module