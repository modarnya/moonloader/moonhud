local module = {}

module.galaxy = require("server.galaxy")

function module.init()
      local ip = sampGetCurrentServerAddress()

      if module.galaxy.ip[ip] then
            is_galaxy = true
            module.galaxy.init()
      end
end

function module.load_config()
      if is_galaxy then
            module.galaxy.load_config()
      end
end

function module.save_config()
      if is_galaxy then
            local success = module.galaxy.save_config()

            add_chat_message(
                  success
                  and "Galaxy-RPG configuration saved {5BFF83}successfully"
                  or "Galaxy-RPG configuration {FB4343}doesn't want to be saved :("
            )
      end
end

function module.reset_config()
      if is_galaxy then
            module.galaxy.reset_config()
      end
end

function module.on_every_frame()
      if is_galaxy then
            module.galaxy.render()
      end
end

function imgui.ServersFeaturesConfiguration()
      if is_galaxy then
            imgui.GalaxyFeaturesConfiguration(module.galaxy.config)
      end
end

function imgui.ServersFeatureSelectBox()
      if is_galaxy then
            imgui.GalaxyFeatureSelectBox()
      end
end

return module