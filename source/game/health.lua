local module = {}

local MAX_HEALTH = 100
local HEALTH_LIMIT = 160

local function copy_player_current_color(config)

end

function module.init_hooks(config)
      if config.HEALTH.copy_clist.v then
            local _, player_id = sampGetPlayerIdByCharHandle(PLAYER_PED)
            local _, r, g, b = hex_to_argb(sampGetPlayerColor(player_id))
            config.HEALTH.main_color = imgui.ImFloat4(imgui.ImColor(r, g, b, math.floor(config.HEALTH.main_color.v[4] * 0xFF)):GetFloat4())
            config.HEALTH.background_color = imgui.ImFloat4(imgui.ImColor(math.floor(r / 4), math.floor(g / 4), math.floor(b / 4), math.floor(config.HEALTH.background_color.v[4] * 0xFF)):GetFloat4())
      end

      samp_events.onSetPlayerColor = function(id, color)
            if not config.HEALTH.copy_clist.v then
                  return
            end

            local _, player_id = sampGetPlayerIdByCharHandle(PLAYER_PED)

            if id == player_id then
                  local r, g, b, _ = hex_to_argb(color)
                  config.HEALTH.main_color = imgui.ImFloat4(imgui.ImColor(r, g, b, math.floor(config.HEALTH.main_color.v[4] * 0xFF)):GetFloat4())
                  config.HEALTH.background_color = imgui.ImFloat4(imgui.ImColor(math.floor(r / 4), math.floor(g / 4), math.floor(b / 4), math.floor(config.HEALTH.background_color.v[4] * 0xFF)):GetFloat4())
            end
      end
end

function module.render(config)
      if not config.HEALTH.on.v then
            return
      end

      local health = getCharHealth(PLAYER_PED)

      if config.HEALTH.bar.v then
            local ratio = clamp(health / MAX_HEALTH, 0, 1)
            render_simple_borderbox(config.HEALTH, ratio)
      end

      if config.HEALTH.text.v then
            render_simple_textdraw(config.HEALTH, tostring(health))
      end
end

return module