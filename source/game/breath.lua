local module = {}

local MAX_BREATH = 100

function module.render(config)
      if not config.BREATHE.on.v then
            return
      end

      if not isCharInWater(PLAYER_PED) and config.BREATHE.disable_on_foot.v then
            return
      end

      local breath = math.floor(memory.getfloat(0xB7CDE0) / 39.97000244)

      if config.BREATHE.bar.v then
            local ratio = breath / MAX_BREATH
            render_simple_borderbox(config.BREATHE, ratio)
      end

      if config.BREATHE.text.v then
            local text = tostring(breath)
            render_simple_textdraw(config.BREATHE, text)
      end
end

return module