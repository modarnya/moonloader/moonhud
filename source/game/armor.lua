local module = {}

local MAX_ARMOR = 100

function module.render(config)
      if not config.ARMOR.on.v then
            return
      end

      local armor = getCharArmour(PLAYER_PED)

      if armor > 0 or not config.ARMOR.zero_disable.v then
      end

      if config.ARMOR.bar.v then
            local ratio = armor / MAX_ARMOR
            render_simple_borderbox(config.ARMOR, ratio)
      end

      if config.ARMOR.text.v then
            local text = tostring(armor)
            render_simple_textdraw(config.ARMOR, text)
      end
end

return module