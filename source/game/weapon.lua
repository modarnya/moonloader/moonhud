local module = {}

local function should_ammo_be_rendered(current_weapon)
      if current_weapon > 15 and current_weapon < 40 then
            return true
      end

      if current_weapon > 40 and current_weapon < 44 then
            return true
      end

      return false
end

local function get_ammo_string(weapon)
      local ammo_count = getAmmoInCharWeapon(PLAYER_PED, weapon)

      local cweapon = getCharPointer(PLAYER_PED) + 0x5A0
      local current_cweapon = cweapon + getWeapontypeSlot(weapon) * 0x1C
      local ammo_in_clip = memory.getuint32(current_cweapon + 0x8)

      return string.format('%d-%d', ammo_count - ammo_in_clip, ammo_in_clip)
end

function module.init_resources()
      for i = 0, 46 do
            textures[i] = moon_additions.load_png_texture(RESOURCE_PATH .. "/weapon-icons/default-hd/" .. i .. ".png")
      end
end

function module.render(config)
      if not config.WEAPON.on.v then
            return
      end

      local weapon = getCurrentCharWeapon(PLAYER_PED)

      if config.WEAPON.icon.v and textures[weapon] then
            render_simple_texture(config.WEAPON, textures[weapon])
      end

      if config.WEAPON.text.v and should_ammo_be_rendered(weapon) then
            local text = get_ammo_string(weapon)
            render_simple_textdraw(config.WEAPON, text)
      end
end

return module