local module = {}

-- this module cannot be refactored and can cause severe brain damage

kill_list_font = nil
local kill_list_icons = {}
kill_lines = {}

function module.init_resources(config)
      kill_list_font = renderCreateFont(config.KILLLIST.font_name, config.KILLLIST.font_size.v, config.KILLLIST.font_flag.v)
      renderFontDrawText(kill_list_font, "moon 0.26.5 bug", 9999, 9999, -1)

      -- 💀💀💀💀💀💀
      for i = 0, 225 do
            if i >= 0 and i <= 18 or i >= 22 and i <= 47 or i >= 49 and i <= 51 or i == 53 or i == 54 or i == 200 or i == 201 or i == 225 then
                  local path = RESOURCE_PATH .. "/list-icons/original-filled/" .. i .. ".png"
                  if doesFileExist(path) then
                        kill_list_icons[i] = renderLoadTextureFromFile(path)
                  end
            end
      end
end

function module.init_hooks(config)
      samp_events.onPlayerDeathNotification = function(killerId, killedId, reason)
            if reason < 0 or reason > 46 or killerId > 1000 or killedId > 1000 then
                  return false
            end

            lua_thread.create(function()
                  while isGamePaused() do
                        wait(0)
                  end

                  local Line = {
                        killer = (not config.KILLLIST.show_id.v) and sampGetPlayerNickname(killerId) or string.format("%s[%d]", sampGetPlayerNickname(killerId), killerId),
                        killerColor = sampGetPlayerColor(killerId),
                        killed = (not config.KILLLIST.show_id.v) and sampGetPlayerNickname(killedId) or string.format("%s[%d]", sampGetPlayerNickname(killedId), killedId),
                        killedColor = sampGetPlayerColor(killedId),
                        reason = reason
                  }
                  if (#kill_lines >= config.KILLLIST.lines.v) then
                        kill_lines[#kill_lines + 1] = Line
                        for i = 1, #kill_lines - 1 do
                              kill_lines[i] = kill_lines[i + 1]
                        end
                        table.remove(kill_lines)
                  else
                        table.insert(kill_lines, Line)
                  end
            end)
      end
end

function module.render(config)
      if not config.KILLLIST.on.v then
            return
      end

      for i = 1, #kill_lines do
            local pos_y = config.KILLLIST.pos_y.v + math.pow(-1, config.KILLLIST.reverse.v) * (i - 1) * (config.KILLLIST.indent_y.v + renderGetFontDrawHeight(kill_list_font))

            if config.KILLLIST.alignment.v == 0 then
                  if config.KILLLIST.icon_pos.v == 0 then
                        renderDrawTexture(
                              kill_list_icons[kill_lines[i].reason],
                              config.KILLLIST.pos_x.v,
                              pos_y,
                              config.KILLLIST.icon_size.v,
                              config.KILLLIST.icon_size.v,
                              0,
                              4294967295.0
                        )

                        renderFontDrawText(
                              kill_list_font,
                              string.format('{%06X}%s {ffffff}> {%06X}%s', argb_to_rgb_bare(kill_lines[i].killerColor), kill_lines[i].killer, argb_to_rgb_bare(kill_lines[i].killedColor), kill_lines[i].killed),
                              config.KILLLIST.pos_x.v + config.KILLLIST.indent_x.v + config.KILLLIST.icon_size.v,
                              pos_y,
                              -1
                        )
                  elseif config.KILLLIST.icon_pos.v == 1 then
                        renderFontDrawText(
                              kill_list_font,
                              string.format('{%06X}%s', argb_to_rgb_bare(kill_lines[i].killerColor), kill_lines[i].killer),
                              config.KILLLIST.pos_x.v,
                              pos_y,
                              -1
                        )

                        renderDrawTexture(
                              kill_list_icons[kill_lines[i].reason],
                              config.KILLLIST.pos_x.v + renderGetFontDrawTextLength(kill_list_font, kill_lines[i].killer) + config.KILLLIST.indent_x.v,
                              pos_y,
                              config.KILLLIST.icon_size.v,
                              config.KILLLIST.icon_size.v,
                              0,
                              4294967295.0
                        )

                        renderFontDrawText(
                              kill_list_font,
                              string.format('{%06X}%s', argb_to_rgb_bare(kill_lines[i].killedColor), kill_lines[i].killed),
                              config.KILLLIST.pos_x.v + renderGetFontDrawTextLength(kill_list_font, kill_lines[i].killer) + 2 * config.KILLLIST.indent_x.v + config.KILLLIST.icon_size.v,
                              pos_y,
                              -1
                        )
                  elseif config.KILLLIST.icon_pos.v == 2 then
                        renderFontDrawText(
                              kill_list_font,
                              string.format('{%06X}%s {ffffff}> {%06X}%s', argb_to_rgb_bare(kill_lines[i].killerColor), kill_lines[i].killer, argb_to_rgb_bare(kill_lines[i].killedColor), kill_lines[i].killed),
                              config.KILLLIST.pos_x.v,
                              pos_y,
                              -1
                        )

                        renderDrawTexture(
                              kill_list_icons[kill_lines[i].reason],
                              config.KILLLIST.pos_x.v + config.KILLLIST.indent_x.v + renderGetFontDrawTextLength(kill_list_font, string.format('%s > %s', kill_lines[i].killer, kill_lines[i].killed)),
                              pos_y,
                              config.KILLLIST.icon_size.v,
                              config.KILLLIST.icon_size.v,
                              0,
                              4294967295.0
                        )
                  end
            elseif config.KILLLIST.alignment.v == 1 then
                  if config.KILLLIST.icon_pos.v == 0 then
                        renderFontDrawText(
                              kill_list_font,
                              string.format('{%06X}%s {ffffff}>', argb_to_rgb_bare(kill_lines[i].killerColor), kill_lines[i].killer),
                              config.KILLLIST.pos_x.v - renderGetFontDrawTextLength(kill_list_font, kill_lines[i].killer) - renderGetFontDrawTextLength(kill_list_font, ' >') / 2,
                              pos_y,
                              -1
                        )

                        renderFontDrawText(
                              kill_list_font,
                              string.format(' {%06X}%s', argb_to_rgb_bare(kill_lines[i].killedColor), kill_lines[i].killed),
                              config.KILLLIST.pos_x.v + renderGetFontDrawTextLength(kill_list_font, '>') / 2,
                              pos_y,
                              -1
                        )

                        renderDrawTexture(
                              kill_list_icons[kill_lines[i].reason],
                              config.KILLLIST.pos_x.v - renderGetFontDrawTextLength(kill_list_font, kill_lines[i].killer) - renderGetFontDrawTextLength(kill_list_font, ' >') / 2 - config.KILLLIST.indent_x.v - config.KILLLIST.icon_size.v,
                              pos_y,
                              config.KILLLIST.icon_size.v,
                              config.KILLLIST.icon_size.v,
                              0,
                              4294967295.0
                        )
                  elseif config.KILLLIST.icon_pos.v == 1 then
                        renderFontDrawText(
                              kill_list_font,
                              string.format('{%06X}%s', argb_to_rgb_bare(kill_lines[i].killerColor), kill_lines[i].killer),
                              config.KILLLIST.pos_x.v - renderGetFontDrawTextLength(kill_list_font, kill_lines[i].killer) - config.KILLLIST.icon_size.v / 2 - config.KILLLIST.indent_x.v,
                              pos_y,
                              -1
                        )

                        renderFontDrawText(
                              kill_list_font,
                              string.format('{%06X}%s', argb_to_rgb_bare(kill_lines[i].killedColor), kill_lines[i].killed),
                              config.KILLLIST.pos_x.v + config.KILLLIST.icon_size.v / 2 + config.KILLLIST.indent_x.v,
                              pos_y,
                              -1
                        )

                        renderDrawTexture(
                              kill_list_icons[kill_lines[i].reason],
                              config.KILLLIST.pos_x.v - config.KILLLIST.icon_size.v / 2,
                              pos_y,
                              config.KILLLIST.icon_size.v,
                              config.KILLLIST.icon_size.v,
                              0,
                              4294967295.0
                        )
                  elseif config.KILLLIST.icon_pos.v == 2 then
                        renderFontDrawText(
                              kill_list_font,
                              string.format('{%06X}%s {ffffff}>', argb_to_rgb_bare(kill_lines[i].killerColor), kill_lines[i].killer),
                              config.KILLLIST.pos_x.v - renderGetFontDrawTextLength(kill_list_font, kill_lines[i].killer) - renderGetFontDrawTextLength(kill_list_font, ' >') / 2,
                              pos_y,
                              -1
                        )

                        renderFontDrawText(
                              kill_list_font,
                              string.format(' {%06X}%s', argb_to_rgb_bare(kill_lines[i].killedColor), kill_lines[i].killed),
                              config.KILLLIST.pos_x.v + renderGetFontDrawTextLength(kill_list_font, '>') / 2,
                              pos_y,
                              -1
                        )

                        renderDrawTexture(
                              kill_list_icons[kill_lines[i].reason],
                              config.KILLLIST.pos_x.v + renderGetFontDrawTextLength(kill_list_font, '>') / 2 + renderGetFontDrawTextLength(kill_list_font, string.format(' %s', kill_lines[i].killed)) + config.KILLLIST.indent_x.v,
                              pos_y,
                              config.KILLLIST.icon_size.v,
                              config.KILLLIST.icon_size.v,
                              0,
                              4294967295.0
                        )
                  end
            elseif config.KILLLIST.alignment.v == 2 then
                  if config.KILLLIST.icon_pos.v == 0 then
                        renderFontDrawText(
                              kill_list_font,
                              string.format('{%06X}%s {ffffff}> {%06X}%s', argb_to_rgb_bare(kill_lines[i].killerColor), kill_lines[i].killer, argb_to_rgb_bare(kill_lines[i].killedColor), kill_lines[i].killed),
                              config.KILLLIST.pos_x.v - renderGetFontDrawTextLength(kill_list_font, string.format('%s > %s', kill_lines[i].killer, kill_lines[i].killed)),
                              pos_y,
                              -1
                        )

                        renderDrawTexture(
                              kill_list_icons[kill_lines[i].reason],
                              config.KILLLIST.pos_x.v - renderGetFontDrawTextLength(kill_list_font, string.format('%s > %s', kill_lines[i].killer, kill_lines[i].killed)) - config.KILLLIST.indent_x.v - config.KILLLIST.icon_size.v,
                              pos_y,
                              config.KILLLIST.icon_size.v,
                              config.KILLLIST.icon_size.v,
                              0,
                              4294967295.0
                        )
                  elseif config.KILLLIST.icon_pos.v == 1 then
                        renderFontDrawText(
                              kill_list_font,
                              string.format('{%06X}%s', argb_to_rgb_bare(kill_lines[i].killerColor), kill_lines[i].killer),
                              config.KILLLIST.pos_x.v - config.KILLLIST.icon_size.v - 2 * config.KILLLIST.indent_x.v - renderGetFontDrawTextLength(kill_list_font, kill_lines[i].killed) - renderGetFontDrawTextLength(kill_list_font, kill_lines[i].killer),
                              pos_y,
                              -1
                        )

                        renderDrawTexture(
                              kill_list_icons[kill_lines[i].reason],
                              config.KILLLIST.pos_x.v - config.KILLLIST.icon_size.v - config.KILLLIST.indent_x.v - renderGetFontDrawTextLength(kill_list_font, kill_lines[i].killed),
                              pos_y,
                              config.KILLLIST.icon_size.v,
                              config.KILLLIST.icon_size.v,
                              0,
                              4294967295.0
                        )

                        renderFontDrawText(
                              kill_list_font,
                              string.format('{%06X}%s', argb_to_rgb_bare(kill_lines[i].killedColor), kill_lines[i].killed),
                              config.KILLLIST.pos_x.v - renderGetFontDrawTextLength(kill_list_font, kill_lines[i].killed),
                              pos_y,
                              -1
                        )
                  elseif config.KILLLIST.icon_pos.v == 2 then
                        renderFontDrawText(
                        kill_list_font,
                        string.format('{%06X}%s {ffffff}> {%06X}%s', argb_to_rgb_bare(kill_lines[i].killerColor), kill_lines[i].killer, argb_to_rgb_bare(kill_lines[i].killedColor), kill_lines[i].killed),
                        config.KILLLIST.pos_x.v - renderGetFontDrawTextLength(kill_list_font, string.format('%s > %s', kill_lines[i].killer, kill_lines[i].killed)) - config.KILLLIST.indent_x.v - config.KILLLIST.icon_size.v,
                        pos_y,
                        -1)

                        renderDrawTexture(
                        kill_list_icons[kill_lines[i].reason],
                        config.KILLLIST.pos_x.v - config.KILLLIST.icon_size.v,
                        pos_y,
                        config.KILLLIST.icon_size.v,
                        config.KILLLIST.icon_size.v,
                        0,
                        4294967295.0)
                  end
            end
      end
end

return module