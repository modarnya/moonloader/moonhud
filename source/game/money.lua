local module = {}

local MONEY_FORMAT_LEADING_ZEROS = "%s$%09d"
local MONEY_FORMAT_TRIMMED = "%s$%d"

function module.render(config)
      if not config.MONEY.on.v then
            return
      end

      local money = getPlayerMoney(PLAYER_HANDLE)

      local text, left, top, width, height
      local text_color, shadow_color, style, align, outline

      local format = config.MONEY.zeros.v and MONEY_FORMAT_LEADING_ZEROS or MONEY_FORMAT_TRIMMED
      local minus_sign = money >= 0 and "" or "-"

      text = string.format(format, minus_sign, money)
      text_color = money < 0 and config.MONEY.text_color_negative.v or config.MONEY.text_color_positive.v

      left = config.MONEY.pos_x.v
      top = config.MONEY.pos_y.v
      width = config.MONEY.scale_x.v
      height = config.MONEY.scale_y.v

      shadow_color = config.MONEY.shadow_color.v
      style = config.MONEY.style.v
      align = config.MONEY.align.v
      outline = 2
      -- no outline config param exists now so rendering as 2 (as usual money)

      render_text(text, left, top, width, height, text_color, shadow_color, style, align, outline)
end

return module