local module = {}

function module.get_level()
      return memory.getuint8(5823328)
end

function module.init_resources()
      textures.star_inactive = moon_additions.load_png_texture(RESOURCE_PATH .. "/wanted-icons/black.png")
      textures.star_active = moon_additions.load_png_texture(RESOURCE_PATH .. "/wanted-icons/white.png")
end

function module.render(config)
      if not config.WANTED.on.v then
            return
      end

      local wanted = module.get_level()

      if wanted == 0 then
            return
      end

      for i = 1, 6 do
            local texture, left, top, right, bottom, color

            left = config.WANTED.pos_x.v - (i - 1) * (config.WANTED.icon_size.v + config.WANTED.indent.v)
            top = config.WANTED.pos_y.v
            right = config.WANTED.pos_x.v - (i - 1) * (config.WANTED.icon_size.v + config.WANTED.indent.v) + config.WANTED.icon_size.v
            bottom = config.WANTED.pos_y.v + config.WANTED.icon_size.v

            if i > wanted then
                  texture = textures.star_inactive

                  local a = config.WANTED.icon_color.v[4] * 0xFF
                  color = imgui.ImFloat4(imgui.ImColor(0xFF, 0xFF, 0xFF, a):GetFloat4()).v
            else
                  texture = textures.star_active
                  color = config.WANTED.icon_color.v
            end

            render_texture(texture, left, top, right, bottom, color)
      end
end

return module