local module = {}

function module.to(config)
      local a, r, g, b = nil
      config.MONEY.on = imgui.ImBool(config.MONEY.on)
      config.MONEY.pos_x = imgui.ImInt(config.MONEY.pos_x)
      config.MONEY.pos_y = imgui.ImInt(config.MONEY.pos_y)
      config.MONEY.style = imgui.ImInt(config.MONEY.style)
      config.MONEY.scale_x = imgui.ImFloat(config.MONEY.scale_x)
      config.MONEY.scale_y = imgui.ImFloat(config.MONEY.scale_y)
      config.MONEY.align = imgui.ImInt(config.MONEY.align)
      a, r, g, b = hex_to_argb(config.MONEY.text_color_positive)
      config.MONEY.text_color_positive = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.MONEY.text_color_negative)
      config.MONEY.text_color_negative = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.MONEY.shadow_color)
      config.MONEY.shadow_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      config.MONEY.zeros = imgui.ImBool(config.MONEY.zeros)

      config.WEAPON.on = imgui.ImBool(config.WEAPON.on)

      config.WEAPON.icon = imgui.ImBool(config.WEAPON.icon)
      config.WEAPON.icon_pos_x = imgui.ImInt(config.WEAPON.icon_pos_x)
      config.WEAPON.icon_pos_y = imgui.ImInt(config.WEAPON.icon_pos_y)
      config.WEAPON.icon_size = imgui.ImInt(config.WEAPON.icon_size)
      a, r, g, b = hex_to_argb(config.WEAPON.icon_color)
      config.WEAPON.icon_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())

      config.WEAPON.text = imgui.ImBool(config.WEAPON.text)
      config.WEAPON.pos_x = imgui.ImInt(config.WEAPON.pos_x)
      config.WEAPON.pos_y = imgui.ImInt(config.WEAPON.pos_y)
      config.WEAPON.style = imgui.ImInt(config.WEAPON.style)
      config.WEAPON.scale_x = imgui.ImFloat(config.WEAPON.scale_x)
      config.WEAPON.scale_y = imgui.ImFloat(config.WEAPON.scale_y)
      config.WEAPON.align = imgui.ImInt(config.WEAPON.align)
      a, r, g, b = hex_to_argb(config.WEAPON.text_color)
      config.WEAPON.text_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.WEAPON.shadow_color)
      config.WEAPON.shadow_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())


      config.WANTED.on = imgui.ImBool(config.WANTED.on)
      config.WANTED.pos_x = imgui.ImInt(config.WANTED.pos_x)
      config.WANTED.pos_y = imgui.ImInt(config.WANTED.pos_y)
      config.WANTED.icon_size = imgui.ImInt(config.WANTED.icon_size)
      config.WANTED.indent = imgui.ImInt(config.WANTED.indent)
      a, r, g, b = hex_to_argb(config.WANTED.icon_color)
      config.WANTED.icon_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())

      config.HEALTH.on = imgui.ImBool(config.HEALTH.on)

      config.HEALTH.bar = imgui.ImBool(config.HEALTH.bar)
      config.HEALTH.bar_pos_x = imgui.ImInt(config.HEALTH.bar_pos_x)
      config.HEALTH.bar_pos_y = imgui.ImInt(config.HEALTH.bar_pos_y)
      config.HEALTH.bar_size_x = imgui.ImInt(config.HEALTH.bar_size_x)
      config.HEALTH.bar_size_y = imgui.ImInt(config.HEALTH.bar_size_y)
      config.HEALTH.border_size = imgui.ImInt(config.HEALTH.border_size)

      config.HEALTH.copy_clist = imgui.ImBool(config.HEALTH.copy_clist)
      a, r, g, b = hex_to_argb(config.HEALTH.main_color)
      config.HEALTH.main_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.HEALTH.border_color)
      config.HEALTH.border_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.HEALTH.background_color)
      config.HEALTH.background_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())

      config.HEALTH.text = imgui.ImBool(config.HEALTH.text)
      config.HEALTH.pos_x = imgui.ImInt(config.HEALTH.pos_x)
      config.HEALTH.pos_y = imgui.ImInt(config.HEALTH.pos_y)
      config.HEALTH.style = imgui.ImInt(config.HEALTH.style)
      config.HEALTH.scale_x = imgui.ImFloat(config.HEALTH.scale_x)
      config.HEALTH.scale_y = imgui.ImFloat(config.HEALTH.scale_y)
      config.HEALTH.align = imgui.ImInt(config.HEALTH.align)
      a, r, g, b = hex_to_argb(config.HEALTH.text_color)
      config.HEALTH.text_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.HEALTH.shadow_color)
      config.HEALTH.shadow_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())


      config.ARMOR.on = imgui.ImBool(config.ARMOR.on)
      config.ARMOR.zero_disable = imgui.ImBool(config.ARMOR.zero_disable)

      config.ARMOR.bar = imgui.ImBool(config.ARMOR.bar)
      config.ARMOR.bar_pos_x = imgui.ImInt(config.ARMOR.bar_pos_x)
      config.ARMOR.bar_pos_y = imgui.ImInt(config.ARMOR.bar_pos_y)
      config.ARMOR.bar_size_x = imgui.ImInt(config.ARMOR.bar_size_x)
      config.ARMOR.bar_size_y = imgui.ImInt(config.ARMOR.bar_size_y)
      config.ARMOR.border_size = imgui.ImInt(config.ARMOR.border_size)
      a, r, g, b = hex_to_argb(config.ARMOR.main_color)
      config.ARMOR.main_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.ARMOR.border_color)
      config.ARMOR.border_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.ARMOR.background_color)
      config.ARMOR.background_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())

      config.ARMOR.text = imgui.ImBool(config.ARMOR.text)
      config.ARMOR.pos_x = imgui.ImInt(config.ARMOR.pos_x)
      config.ARMOR.pos_y = imgui.ImInt(config.ARMOR.pos_y)
      config.ARMOR.style = imgui.ImInt(config.ARMOR.style)
      config.ARMOR.scale_x = imgui.ImFloat(config.ARMOR.scale_x)
      config.ARMOR.scale_y = imgui.ImFloat(config.ARMOR.scale_y)
      config.ARMOR.align = imgui.ImInt(config.ARMOR.align)
      a, r, g, b = hex_to_argb(config.ARMOR.text_color)
      config.ARMOR.text_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.ARMOR.shadow_color)
      config.ARMOR.shadow_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())


      config.SPRINT.on = imgui.ImBool(config.SPRINT.on)
      config.SPRINT.disable_in_water = imgui.ImBool(config.SPRINT.disable_in_water)

      config.SPRINT.bar = imgui.ImBool(config.SPRINT.bar)
      config.SPRINT.bar_pos_x = imgui.ImInt(config.SPRINT.bar_pos_x)
      config.SPRINT.bar_pos_y = imgui.ImInt(config.SPRINT.bar_pos_y)
      config.SPRINT.bar_size_x = imgui.ImInt(config.SPRINT.bar_size_x)
      config.SPRINT.bar_size_y = imgui.ImInt(config.SPRINT.bar_size_y)
      config.SPRINT.border_size = imgui.ImInt(config.SPRINT.border_size)
      a, r, g, b = hex_to_argb(config.SPRINT.main_color)
      config.SPRINT.main_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.SPRINT.border_color)
      config.SPRINT.border_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.SPRINT.background_color)
      config.SPRINT.background_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())

      config.SPRINT.text = imgui.ImBool(config.SPRINT.text)
      config.SPRINT.pos_x = imgui.ImInt(config.SPRINT.pos_x)
      config.SPRINT.pos_y = imgui.ImInt(config.SPRINT.pos_y)
      config.SPRINT.style = imgui.ImInt(config.SPRINT.style)
      config.SPRINT.scale_x = imgui.ImFloat(config.SPRINT.scale_x)
      config.SPRINT.scale_y = imgui.ImFloat(config.SPRINT.scale_y)
      config.SPRINT.align = imgui.ImInt(config.SPRINT.align)
      a, r, g, b = hex_to_argb(config.SPRINT.text_color)
      config.SPRINT.text_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.SPRINT.shadow_color)
      config.SPRINT.shadow_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())


      config.BREATHE.on = imgui.ImBool(config.BREATHE.on)
      config.BREATHE.disable_on_foot = imgui.ImBool(config.BREATHE.disable_on_foot)

      config.BREATHE.bar = imgui.ImBool(config.BREATHE.bar)
      config.BREATHE.bar_pos_x = imgui.ImInt(config.BREATHE.bar_pos_x)
      config.BREATHE.bar_pos_y = imgui.ImInt(config.BREATHE.bar_pos_y)
      config.BREATHE.bar_size_x = imgui.ImInt(config.BREATHE.bar_size_x)
      config.BREATHE.bar_size_y = imgui.ImInt(config.BREATHE.bar_size_y)
      config.BREATHE.border_size = imgui.ImInt(config.BREATHE.border_size)
      a, r, g, b = hex_to_argb(config.BREATHE.main_color)
      config.BREATHE.main_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.BREATHE.border_color)
      config.BREATHE.border_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.BREATHE.background_color)
      config.BREATHE.background_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())

      config.BREATHE.text = imgui.ImBool(config.BREATHE.text)
      config.BREATHE.pos_x = imgui.ImInt(config.BREATHE.pos_x)
      config.BREATHE.pos_y = imgui.ImInt(config.BREATHE.pos_y)
      config.BREATHE.style = imgui.ImInt(config.BREATHE.style)
      config.BREATHE.scale_x = imgui.ImFloat(config.BREATHE.scale_x)
      config.BREATHE.scale_y = imgui.ImFloat(config.BREATHE.scale_y)
      config.BREATHE.align = imgui.ImInt(config.BREATHE.align)
      a, r, g, b = hex_to_argb(config.BREATHE.text_color)
      config.BREATHE.text_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())
      a, r, g, b = hex_to_argb(config.BREATHE.shadow_color)
      config.BREATHE.shadow_color = imgui.ImFloat4(imgui.ImColor(r, g, b, a):GetFloat4())

      config.RADAR.pos_x = imgui.ImInt(config.RADAR.pos_x)
      config.RADAR.pos_y = imgui.ImInt(config.RADAR.pos_y)
      config.RADAR.size_x = imgui.ImInt(config.RADAR.size_x)
      config.RADAR.size_y = imgui.ImInt(config.RADAR.size_y)

      config.KILLLIST.on = imgui.ImBool(config.KILLLIST.on)
      config.KILLLIST.show_id = imgui.ImBool(config.KILLLIST.show_id)
      config.KILLLIST.pos_x = imgui.ImInt(config.KILLLIST.pos_x)
      config.KILLLIST.pos_y = imgui.ImInt(config.KILLLIST.pos_y)
      config.KILLLIST.indent_x = imgui.ImInt(config.KILLLIST.indent_x)
      config.KILLLIST.indent_y = imgui.ImInt(config.KILLLIST.indent_y)
      config.KILLLIST.alignment = imgui.ImInt(config.KILLLIST.alignment)
      config.KILLLIST.icon_pos = imgui.ImInt(config.KILLLIST.icon_pos)
      config.KILLLIST.icon_size = imgui.ImInt(config.KILLLIST.icon_size)
      config.KILLLIST.lines = imgui.ImInt(config.KILLLIST.lines)
      config.KILLLIST.font_size = imgui.ImInt(config.KILLLIST.font_size)
      config.KILLLIST.font_flag = imgui.ImInt(config.KILLLIST.font_flag)
      config.KILLLIST.reverse = imgui.ImInt(config.KILLLIST.reverse)

      return config
end

function module.from(config)
      config.MONEY.on = config.MONEY.on.v
      config.MONEY.pos_x = config.MONEY.pos_x.v
      config.MONEY.pos_y = config.MONEY.pos_y.v
      config.MONEY.style = config.MONEY.style.v
      config.MONEY.scale_x = config.MONEY.scale_x.v
      config.MONEY.scale_y = config.MONEY.scale_y.v
      config.MONEY.align = config.MONEY.align.v
      config.MONEY.text_color_positive = imgui.ImColor(imgui.ImColor.FromFloat4(config.MONEY.text_color_positive.v[3], config.MONEY.text_color_positive.v[2], config.MONEY.text_color_positive.v[1], config.MONEY.text_color_positive.v[4]):GetVec4()):GetU32()
      config.MONEY.text_color_negative = imgui.ImColor(imgui.ImColor.FromFloat4(config.MONEY.text_color_negative.v[3], config.MONEY.text_color_negative.v[2], config.MONEY.text_color_negative.v[1], config.MONEY.text_color_negative.v[4]):GetVec4()):GetU32()
      config.MONEY.shadow_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.MONEY.shadow_color.v[3], config.MONEY.shadow_color.v[2], config.MONEY.shadow_color.v[1], config.MONEY.shadow_color.v[4]):GetVec4()):GetU32()
      config.MONEY.zeros = config.MONEY.zeros.v

      config.WEAPON.on = config.WEAPON.on.v

      config.WEAPON.icon = config.WEAPON.icon.v
      config.WEAPON.icon_pos_x = config.WEAPON.icon_pos_x.v
      config.WEAPON.icon_pos_y = config.WEAPON.icon_pos_y.v
      config.WEAPON.icon_size = config.WEAPON.icon_size.v
      config.WEAPON.icon_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.WEAPON.icon_color.v[3], config.WEAPON.icon_color.v[2], config.WEAPON.icon_color.v[1], config.WEAPON.icon_color.v[4]):GetVec4()):GetU32()

      config.WEAPON.text = config.WEAPON.text.v
      config.WEAPON.pos_x = config.WEAPON.pos_x.v
      config.WEAPON.pos_y = config.WEAPON.pos_y.v
      config.WEAPON.style = config.WEAPON.style.v
      config.WEAPON.scale_x = config.WEAPON.scale_x.v
      config.WEAPON.scale_y = config.WEAPON.scale_y.v
      config.WEAPON.align = config.WEAPON.align.v
      config.WEAPON.text_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.WEAPON.text_color.v[3], config.WEAPON.text_color.v[2], config.WEAPON.text_color.v[1], config.WEAPON.text_color.v[4]):GetVec4()):GetU32()
      config.WEAPON.shadow_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.WEAPON.shadow_color.v[3], config.WEAPON.shadow_color.v[2], config.WEAPON.shadow_color.v[1], config.WEAPON.shadow_color.v[4]):GetVec4()):GetU32()


      config.WANTED.on = config.WANTED.on.v
      config.WANTED.pos_x = config.WANTED.pos_x.v
      config.WANTED.pos_y = config.WANTED.pos_y.v
      config.WANTED.icon_size = config.WANTED.icon_size.v
      config.WANTED.indent = config.WANTED.indent.v
      config.WANTED.icon_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.WANTED.icon_color.v[3], config.WANTED.icon_color.v[2], config.WANTED.icon_color.v[1], config.WANTED.icon_color.v[4]):GetVec4()):GetU32()

      config.HEALTH.on = config.HEALTH.on.v

      config.HEALTH.bar = config.HEALTH.bar.v
      config.HEALTH.bar_pos_x = config.HEALTH.bar_pos_x.v
      config.HEALTH.bar_pos_y = config.HEALTH.bar_pos_y.v
      config.HEALTH.bar_size_x = config.HEALTH.bar_size_x.v
      config.HEALTH.bar_size_y = config.HEALTH.bar_size_y.v
      config.HEALTH.border_size = config.HEALTH.border_size.v

      config.HEALTH.copy_clist = config.HEALTH.copy_clist.v
      config.HEALTH.main_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.HEALTH.main_color.v[3], config.HEALTH.main_color.v[2], config.HEALTH.main_color.v[1], config.HEALTH.main_color.v[4]):GetVec4()):GetU32()
      config.HEALTH.border_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.HEALTH.border_color.v[3], config.HEALTH.border_color.v[2], config.HEALTH.border_color.v[1], config.HEALTH.border_color.v[4]):GetVec4()):GetU32()
      config.HEALTH.background_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.HEALTH.background_color.v[3], config.HEALTH.background_color.v[2], config.HEALTH.background_color.v[1], config.HEALTH.background_color.v[4]):GetVec4()):GetU32()

      config.HEALTH.text = config.HEALTH.text.v
      config.HEALTH.pos_x = config.HEALTH.pos_x.v
      config.HEALTH.pos_y = config.HEALTH.pos_y.v
      config.HEALTH.style = config.HEALTH.style.v
      config.HEALTH.scale_x = config.HEALTH.scale_x.v
      config.HEALTH.scale_y = config.HEALTH.scale_y.v
      config.HEALTH.align = config.HEALTH.align.v
      config.HEALTH.text_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.HEALTH.text_color.v[3], config.HEALTH.text_color.v[2], config.HEALTH.text_color.v[1], config.HEALTH.text_color.v[4]):GetVec4()):GetU32()
      config.HEALTH.shadow_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.HEALTH.shadow_color.v[3], config.HEALTH.shadow_color.v[2], config.HEALTH.shadow_color.v[1], config.HEALTH.shadow_color.v[4]):GetVec4()):GetU32()

      config.ARMOR.on = config.ARMOR.on.v
      config.ARMOR.zero_disable = config.ARMOR.zero_disable.v

      config.ARMOR.bar = config.ARMOR.bar.v
      config.ARMOR.bar_pos_x = config.ARMOR.bar_pos_x.v
      config.ARMOR.bar_pos_y = config.ARMOR.bar_pos_y.v
      config.ARMOR.bar_size_x = config.ARMOR.bar_size_x.v
      config.ARMOR.bar_size_y = config.ARMOR.bar_size_y.v
      config.ARMOR.border_size = config.ARMOR.border_size.v
      config.ARMOR.main_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.ARMOR.main_color.v[3], config.ARMOR.main_color.v[2], config.ARMOR.main_color.v[1], config.ARMOR.main_color.v[4]):GetVec4()):GetU32()
      config.ARMOR.border_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.ARMOR.border_color.v[3], config.ARMOR.border_color.v[2], config.ARMOR.border_color.v[1], config.ARMOR.border_color.v[4]):GetVec4()):GetU32()
      config.ARMOR.background_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.ARMOR.background_color.v[3], config.ARMOR.background_color.v[2], config.ARMOR.background_color.v[1], config.ARMOR.background_color.v[4]):GetVec4()):GetU32()

      config.ARMOR.text = config.ARMOR.text.v
      config.ARMOR.pos_x = config.ARMOR.pos_x.v
      config.ARMOR.pos_y = config.ARMOR.pos_y.v
      config.ARMOR.style = config.ARMOR.style.v
      config.ARMOR.scale_x = config.ARMOR.scale_x.v
      config.ARMOR.scale_y = config.ARMOR.scale_y.v
      config.ARMOR.align = config.ARMOR.align.v
      config.ARMOR.text_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.ARMOR.text_color.v[3], config.ARMOR.text_color.v[2], config.ARMOR.text_color.v[1], config.ARMOR.text_color.v[4]):GetVec4()):GetU32()
      config.ARMOR.shadow_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.ARMOR.shadow_color.v[3], config.ARMOR.shadow_color.v[2], config.ARMOR.shadow_color.v[1], config.ARMOR.shadow_color.v[4]):GetVec4()):GetU32()

      config.SPRINT.on = config.SPRINT.on.v
      config.SPRINT.disable_in_water = config.SPRINT.disable_in_water.v

      config.SPRINT.bar = config.SPRINT.bar.v
      config.SPRINT.bar_pos_x = config.SPRINT.bar_pos_x.v
      config.SPRINT.bar_pos_y = config.SPRINT.bar_pos_y.v
      config.SPRINT.bar_size_x = config.SPRINT.bar_size_x.v
      config.SPRINT.bar_size_y = config.SPRINT.bar_size_y.v
      config.SPRINT.border_size = config.SPRINT.border_size.v
      config.SPRINT.main_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.SPRINT.main_color.v[3], config.SPRINT.main_color.v[2], config.SPRINT.main_color.v[1], config.SPRINT.main_color.v[4]):GetVec4()):GetU32()
      config.SPRINT.border_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.SPRINT.border_color.v[3], config.SPRINT.border_color.v[2], config.SPRINT.border_color.v[1], config.SPRINT.border_color.v[4]):GetVec4()):GetU32()
      config.SPRINT.background_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.SPRINT.background_color.v[3], config.SPRINT.background_color.v[2], config.SPRINT.background_color.v[1], config.SPRINT.background_color.v[4]):GetVec4()):GetU32()

      config.SPRINT.text = config.SPRINT.text.v
      config.SPRINT.pos_x = config.SPRINT.pos_x.v
      config.SPRINT.pos_y = config.SPRINT.pos_y.v
      config.SPRINT.style = config.SPRINT.style.v
      config.SPRINT.scale_x = config.SPRINT.scale_x.v
      config.SPRINT.scale_y = config.SPRINT.scale_y.v
      config.SPRINT.align = config.SPRINT.align.v
      config.SPRINT.text_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.SPRINT.text_color.v[3], config.SPRINT.text_color.v[2], config.SPRINT.text_color.v[1], config.SPRINT.text_color.v[4]):GetVec4()):GetU32()
      config.SPRINT.shadow_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.SPRINT.shadow_color.v[3], config.SPRINT.shadow_color.v[2], config.SPRINT.shadow_color.v[1], config.SPRINT.shadow_color.v[4]):GetVec4()):GetU32()

      config.BREATHE.on = config.BREATHE.on.v
      config.BREATHE.disable_on_foot = config.BREATHE.disable_on_foot.v

      config.BREATHE.bar = config.BREATHE.bar.v
      config.BREATHE.bar_pos_x = config.BREATHE.bar_pos_x.v
      config.BREATHE.bar_pos_y = config.BREATHE.bar_pos_y.v
      config.BREATHE.bar_size_x = config.BREATHE.bar_size_x.v
      config.BREATHE.bar_size_y = config.BREATHE.bar_size_y.v
      config.BREATHE.border_size = config.BREATHE.border_size.v
      config.BREATHE.main_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.BREATHE.main_color.v[3], config.BREATHE.main_color.v[2], config.BREATHE.main_color.v[1], config.BREATHE.main_color.v[4]):GetVec4()):GetU32()
      config.BREATHE.border_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.BREATHE.border_color.v[3], config.BREATHE.border_color.v[2], config.BREATHE.border_color.v[1], config.BREATHE.border_color.v[4]):GetVec4()):GetU32()
      config.BREATHE.background_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.BREATHE.background_color.v[3], config.BREATHE.background_color.v[2], config.BREATHE.background_color.v[1], config.BREATHE.background_color.v[4]):GetVec4()):GetU32()

      config.BREATHE.text = config.BREATHE.text.v
      config.BREATHE.pos_x = config.BREATHE.pos_x.v
      config.BREATHE.pos_y = config.BREATHE.pos_y.v
      config.BREATHE.style = config.BREATHE.style.v
      config.BREATHE.scale_x = config.BREATHE.scale_x.v
      config.BREATHE.scale_y = config.BREATHE.scale_y.v
      config.BREATHE.align = config.BREATHE.align.v
      config.BREATHE.text_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.BREATHE.text_color.v[3], config.BREATHE.text_color.v[2], config.BREATHE.text_color.v[1], config.BREATHE.text_color.v[4]):GetVec4()):GetU32()
      config.BREATHE.shadow_color = imgui.ImColor(imgui.ImColor.FromFloat4(config.BREATHE.shadow_color.v[3], config.BREATHE.shadow_color.v[2], config.BREATHE.shadow_color.v[1], config.BREATHE.shadow_color.v[4]):GetVec4()):GetU32()

      config.RADAR.pos_x = config.RADAR.pos_x.v
      config.RADAR.pos_y = config.RADAR.pos_y.v
      config.RADAR.size_x = config.RADAR.size_x.v
      config.RADAR.size_y = config.RADAR.size_y.v

      config.KILLLIST.on = config.KILLLIST.on.v
      config.KILLLIST.show_id = config.KILLLIST.show_id.v
      config.KILLLIST.pos_x = config.KILLLIST.pos_x.v
      config.KILLLIST.pos_y = config.KILLLIST.pos_y.v
      config.KILLLIST.indent_x = config.KILLLIST.indent_x.v
      config.KILLLIST.indent_y = config.KILLLIST.indent_y.v
      config.KILLLIST.alignment = config.KILLLIST.alignment.v
      config.KILLLIST.icon_pos = config.KILLLIST.icon_pos.v
      config.KILLLIST.icon_size = config.KILLLIST.icon_size.v
      config.KILLLIST.lines = config.KILLLIST.lines.v
      config.KILLLIST.font_size = config.KILLLIST.font_size.v
      config.KILLLIST.font_flag = config.KILLLIST.font_flag.v
      config.KILLLIST.reverse = config.KILLLIST.reverse.v

      return config
end

return module