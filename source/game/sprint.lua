local module = {}

local MAX_SPRINT = 100

function module.render(config)
      if not config.SPRINT.on.v then
            return
      end

      if isCharInWater(PLAYER_PED) and config.SPRINT.disable_in_water.v then
            return
      end


      local sprint = math.floor(memory.getfloat(0xB7CDB4) / 31.47000244)

      if config.SPRINT.bar.v then
            local ratio = sprint / MAX_SPRINT
            render_simple_borderbox(config.SPRINT, ratio)
      end

      if config.SPRINT.text.v then
            local text = tostring(sprint)
            render_simple_textdraw(config.SPRINT, text)
      end
end

return module