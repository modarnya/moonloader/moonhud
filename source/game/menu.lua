local GAME_FEATURES = {
      fa.ICON_DOLLAR_SIGN .. "  Money",
      fa.ICON_FIST_RAISED .. "  Weapon",
      fa.ICON_STAR .. "  Wanted",
      fa.ICON_HEART .. "  Health",
      fa.ICON_SHIELD_ALT .. "  Armor",
      fa.ICON_RUNNING .. "  Sprint",
      fa.ICON_MALE .. "  Breathe",
      fa.ICON_ROUTE .. "  Radar",
}
local NATIVE_RESOLUTION_X, NATIVE_RESOLUTION_Y = 640, 448

function imgui.GameFeaturesConfiguration(config)
      if selected == 1 then
            imgui.Checkbox("Enabled", config.MONEY.on)
            imgui.Checkbox("Leading zeros", config.MONEY.zeros)

            imgui.InputInt(fa.ICON_SLIDERS_H .. "  Text position X", config.MONEY.pos_x)
            imgui.InputInt(fa.ICON_SLIDERS_H .. "  Text position Y", config.MONEY.pos_y)
            if imgui.Button(fa.ICON_RETWEET .. "  Choose position", imgui.ImVec2(225, 20)) then
                  select_position_for(config.MONEY.pos_x, config.MONEY.pos_y)
            end
            imgui.SliderFloat(fa.ICON_SLIDERS_H .. "  Width", config.MONEY.scale_x, 0, 20)
            imgui.SliderFloat(fa.ICON_SLIDERS_H .. "  Height", config.MONEY.scale_y, 0, 20)

            imgui.ColorEdit4(fa.ICON_SPINNER .. "  Color regular", config.MONEY.text_color_positive)
            imgui.ColorEdit4(fa.ICON_SPINNER .. "  Color in debt", config.MONEY.text_color_negative)
            imgui.ColorEdit4(fa.ICON_SPINNER .. "  Shadow color", config.MONEY.shadow_color)

            imgui.Text("Style:")
            imgui.RadioButton("SUBTITLES", config.MONEY.style, 1)
            imgui.SameLine()
            imgui.RadioButton("MENU", config.MONEY.style, 2)
            imgui.SameLine()
            imgui.RadioButton("GOTHIC", config.MONEY.style, 3)
            imgui.SameLine()
            imgui.RadioButton("PRICEDOWN", config.MONEY.style, 4)

            imgui.Text("Alignment:")
            imgui.RadioButton("Left", config.MONEY.align, 1)
            imgui.SameLine()
            imgui.RadioButton("Center", config.MONEY.align, 2)
            imgui.SameLine()
            imgui.RadioButton("Right", config.MONEY.align, 3)
      end
      if selected == 2 then
            imgui.Checkbox("Enabled", config.WEAPON.on)
            imgui.SimpleTextureConfiguration("weapon", config.WEAPON)
            imgui.Checkbox("Display text##weapon", config.WEAPON.text)
            imgui.SimpleTextdrawConfiguration("weapon", config.WEAPON)
      end
      if selected == 3 then
            imgui.Checkbox("Enabled", config.WANTED.on)
            imgui.InputInt(fa.ICON_SLIDERS_H .. "  Icon position x", config.WANTED.pos_x)
            imgui.InputInt(fa.ICON_SLIDERS_H .. "  Icon position y", config.WANTED.pos_y)
            if imgui.Button(fa.ICON_RETWEET .. "  Choose icon position", imgui.ImVec2(225, 20)) then
                  select_position_for(config.WANTED.pos_x, config.WANTED.pos_y)
            end
            imgui.SliderInt(fa.ICON_SLIDERS_H .. "  Icon size", config.WANTED.icon_size, 0, 200)
            imgui.ColorEdit4(fa.ICON_SPINNER .. "  Icon color", config.WANTED.icon_color)
            imgui.SliderInt(fa.ICON_SLIDERS_H .. "  Indent", config.WANTED.indent, 0, 100)
      end
      if selected == 4 then
            imgui.Checkbox("Enabled", config.HEALTH.on)
            imgui.SimpleBorderBoxConfiguration("hp", config.HEALTH)
            imgui.Checkbox("Copy SAMP color", config.HEALTH.copy_clist)
            imgui.Checkbox("Display text##hp", config.HEALTH.text)
            imgui.SimpleTextdrawConfiguration("hp", config.HEALTH)
      end
      if selected == 5 then
            imgui.Checkbox("Enabled", config.ARMOR.on)
            imgui.Checkbox("Hide armor points when if no armor", config.ARMOR.zero_disable)
            imgui.SimpleBorderBoxConfiguration("ap", config.ARMOR)
            imgui.Checkbox("Display text##ap", config.ARMOR.text)
            imgui.SimpleTextdrawConfiguration("ap", config.ARMOR)
      end
      if selected == 6 then
            imgui.Checkbox("Enabled", config.SPRINT.on)
            imgui.Checkbox("Hide when in water", config.SPRINT.disable_in_water)
            imgui.SimpleBorderBoxConfiguration("sprint", config.SPRINT)
            imgui.Checkbox("Display text##sprint", config.SPRINT.text)
            imgui.SimpleTextdrawConfiguration("sprint", config.SPRINT)
      end
      if selected == 7 then
            imgui.Checkbox("Enabled", config.BREATHE.on)
            imgui.Checkbox("Hide ashore", config.BREATHE.disable_on_foot)
            imgui.SimpleBorderBoxConfiguration("breath", config.BREATHE)
            imgui.Checkbox("Display text##breath", config.BREATHE.text)
            imgui.SimpleTextdrawConfiguration("breath", config.BREATHE)
      end
      if selected == 8 then
            if imgui.InputInt(fa.ICON_SLIDERS_H .. "  Position X", config.RADAR.pos_x) then
                  set_radar_pos(config.RADAR.pos_x.v, config.RADAR.pos_y.v)
            end
            if imgui.InputInt(fa.ICON_SLIDERS_H .. "  Position Y", config.RADAR.pos_y) then
                  set_radar_pos(config.RADAR.pos_x.v, config.RADAR.pos_y.v)
            end
            if imgui.Button(fa.ICON_RETWEET .. "  Choose position", imgui.ImVec2(225, 20)) then
                  select_position_and(function()
                        local radar_pos_x, radar_pos_y = getCursorPos()
                        local resolution_x, resolution_y = getScreenResolution()

                        config.RADAR.pos_x.v, config.RADAR.pos_y.v = getCursorPos()
                        config.RADAR.pos_x.v = math.floor(radar_pos_x / resolution_x * NATIVE_RESOLUTION_X)
                        config.RADAR.pos_y.v = NATIVE_RESOLUTION_Y - math.floor(radar_pos_y / resolution_y * NATIVE_RESOLUTION_Y)
                        set_radar_pos(config.RADAR.pos_x.v, config.RADAR.pos_y.v)
                  end)
            end
            if imgui.SliderInt(fa.ICON_SLIDERS_H .. "  Width", config.RADAR.size_x, 0, NATIVE_RESOLUTION_X) then
                  set_radar_size(config.RADAR.size_x.v, config.RADAR.size_y.v)
            end
            if imgui.SliderInt(fa.ICON_SLIDERS_H .. "  Height", config.RADAR.size_y, 0, NATIVE_RESOLUTION_Y) then
                  set_radar_size(config.RADAR.size_x.v, config.RADAR.size_y.v)
            end
      end
      if selected == 0 then
            imgui.Checkbox("Enabled", config.KILLLIST.on)
            imgui.Checkbox("Display IDs", config.KILLLIST.show_id)
            imgui.RadioButton("Top to bottom", config.KILLLIST.reverse, 0)
            imgui.RadioButton("Bottom to top", config.KILLLIST.reverse, 1)

            imgui.InputInt(fa.ICON_SLIDERS_H .. "  Position X", config.KILLLIST.pos_x)
            imgui.InputInt(fa.ICON_SLIDERS_H .. "  Position Y", config.KILLLIST.pos_y)
            if imgui.Button(fa.ICON_RETWEET .. " Choose position", imgui.ImVec2(225, 20)) then
                  select_position_for(config.KILLLIST.pos_x, config.KILLLIST.pos_y)
            end
            imgui.SliderInt(fa.ICON_SLIDERS_H .. "  Indent X", config.KILLLIST.indent_x, 0, 100)
            imgui.SliderInt(fa.ICON_SLIDERS_H .. "  Indent Y", config.KILLLIST.indent_y, 0, 100)

            imgui.Text("Alignment:")
            imgui.RadioButton("Left##align", config.KILLLIST.alignment, 0)
            imgui.SameLine()
            imgui.RadioButton("Center##align", config.KILLLIST.alignment, 1)
            imgui.SameLine()
            imgui.RadioButton("Right##align", config.KILLLIST.alignment, 2)
            imgui.Text("Icon:")
            imgui.RadioButton("Left##icon", config.KILLLIST.icon_pos, 0)
            imgui.SameLine()
            imgui.RadioButton("Center##icon", config.KILLLIST.icon_pos, 1)
            imgui.SameLine()
            imgui.RadioButton("Right##icon", config.KILLLIST.icon_pos, 2)

            imgui.SliderInt(fa.ICON_SLIDERS_H .. "  Icon size", config.KILLLIST.icon_size, 1, 200)
            if imgui.SliderInt(fa.ICON_TIMES .. "  Line limit", config.KILLLIST.lines, 1, 20) then
                  if config.KILLLIST.lines.v < #kill_lines then
                        for i = config.KILLLIST.lines.v + 1, #kill_lines do
                              table.remove(kill_lines, i)
                        end
                  end
            end

            imgui.Text(fa.ICON_FONT .. ("  Font Name (%s)"):format(config.KILLLIST.font_name))

            imgui.PushItemWidth(410)
            imgui.InputText("##font", font_buffer)
            imgui.PopItemWidth()
            imgui.SameLine()
            imgui.SetCursorPosX(420)

            if imgui.Button(fa.ICON_CHECK .. "  Confirm", imgui.ImVec2(118, 20)) then
                  config.KILLLIST.font_name = utf8:decode(font_buffer.v)
                  kill_list_font = renderCreateFont(config.KILLLIST.font_name, config.KILLLIST.font_size.v, config.KILLLIST.font_flag.v)
            end

            if imgui.InputInt(fa.ICON_TEXT_WIDTH .. "  Font size", config.KILLLIST.font_size) then
                  config.KILLLIST.font_size.v = clamp(config.KILLLIST.font_size.v, 1, 100)
                  kill_list_font = renderCreateFont(config.KILLLIST.font_name, config.KILLLIST.font_size.v, config.KILLLIST.font_flag.v)
            end

            if imgui.InputInt(fa.ICON_FLAG .. "  Font flags sum", config.KILLLIST.font_flag) then
                  -- to busy to remove this cringe, note for you to know i am aware of it
                  config.KILLLIST.font_flag.v = clamp(config.KILLLIST.font_flag.v, 0, 100)
                  kill_list_font = renderCreateFont(config.KILLLIST.font_name, config.KILLLIST.font_size.v, config.KILLLIST.font_flag.v)
            end
      end
end

function imgui.GameFeaturesSelectBox()
      return imgui.DrawSelectBox(GAME_FEATURES, 100, 75, 1, true, 0)
end