local module = {}

module.config_path = "moonhud/game.ini"
module.get_config = require("game.config")
module.serialization = require("game.serialization")

local armor = require("game.armor")
local breath = require("game.breath")
local health = require("game.health")
local kill_list = require("game.kill_list")
local money = require("game.money")
local radar = require("game.radar")
local sprint = require("game.sprint")
local wanted = require("game.wanted")
local weapon = require("game.weapon")

require("game.menu")

function module.load_config()
      module.config = inicfg.load(module.get_config(), module.config_path)

      if doesFileExist(module.config_path) then
            inicfg.save(module.config)
      end

      module.config = module.serialization.to(module.config)
end

function module.save_config()
      local success = false

      module.config = module.serialization.from(module.config)
      success = inicfg.save(module.config, module.config_path)
      module.config = module.serialization.to(module.config)

      return success
end

function module.reset_config()
      module.config = module.get_config()
      module.config = module.serialization.to(module.config)
end

function module.init()
      module.load_config()

      wanted.init_resources()
      weapon.init_resources()
      kill_list.init_resources(module.config)

      kill_list.init_hooks(module.config)
      health.init_hooks(module.config)
end

function module.on_every_frame()
      armor.render(module.config)
      breath.render(module.config)
      health.render(module.config)
      kill_list.render(module.config)
      money.render(module.config)
      sprint.render(module.config)
      wanted.render(module.config)
      weapon.render(module.config)
end

function imgui.FeaturesConfiguration()
      imgui.GameFeaturesConfiguration(module.config)
end

function imgui.FeatureSelectBox()
      imgui.GameFeaturesSelectBox()
end

return module