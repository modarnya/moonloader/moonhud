function argb_to_rgb_bare(argb)
	return bit.band(argb, 0xFFFFFF)
end

function argb_to_rgb(argb, new_a)
      local a, r, g, b = hex_to_argb(argb)

	return argb_to_hex(new_a, r, g, b)
end

function argb_to_hex(a, r, g, b)
      local argb = b

      argb = bit.bor(argb, bit.lshift(g, 8))
      argb = bit.bor(argb, bit.lshift(r, 16))
      argb = bit.bor(argb, bit.lshift(a, 24))

      return argb
end

function hex_to_argb(hex)
      local a = bit.band(bit.rshift(hex, 24), 0xFF)
      local r = bit.band(bit.rshift(hex, 16), 0xFF)
      local g = bit.band(bit.rshift(hex, 8), 0xFF)
      local b = bit.band(hex, 0xFF)

      return a, r, g, b
end

function imfloat4_to_argb(imvec)
      local r = math.floor(imvec[1] * 0xFF)
      local g = math.floor(imvec[2] * 0xFF)
      local b = math.floor(imvec[3] * 0xFF)
      local a = math.floor(imvec[4] * 0xFF)

      return a, r, g, b
end