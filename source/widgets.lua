function imgui.MenuOption(name, on, size)
      local color, colorHovered, colorActive

      color = on and imgui.ImVec4(1.00, 0.28, 0.28, 1.00) or imgui.ImVec4(0.24, 0.24, 0.24, 1)
      colorHovered = imgui.ImVec4(0.5, 0.14, 0.14, 1.00)
      colorActive = imgui.ImVec4(0.25, 0.07, 0.07, 1)

      return imgui.CustomButton(name, color, colorHovered, colorActive, size)
end

function imgui.OnOffSwitch(on)
      local label, color, colorHovered, colorActive

      label = fa.ICON_POWER_OFF .. (on and "  ON" or "  OFF")
      color = on and imgui.ImVec4(0.14, 0.5, 0.14, 1.00) or imgui.ImVec4(0.5, 0.14, 0.14, 1.00)
      colorHovered = on and imgui.ImVec4(0.15, 0.59, 0.18, 0.5) or imgui.ImVec4(1, 0.19, 0.19, 0.3)
      colorActive = on and imgui.ImVec4(0.15, 0.59, 0.18, 0.4) or imgui.ImVec4(1, 0.19, 0.19, 0.2)

      return  imgui.CustomButton(label, color, colorHovered, colorActive, imgui.ImVec2(161, 75))
end

function imgui.BigControlButton(label)
      local color, colorHovered, colorActive, size

      color = imgui.ImVec4(0.24, 0.24, 0.24, 1)
      colorHovered = imgui.ImVec4(0.5, 0.14, 0.14, 1.00)
      colorActive = imgui.ImVec4(0.25, 0.07, 0.07, 1)
      size = imgui.ImVec2(161, 75)

      return imgui.CustomButton(label, color, colorHovered, colorActive, size)
end

-- look he learned the ternary operator how cool now try to refactor line 20 :)))
function imgui.DrawSelectBox(arr, button_size_x, button_size_y, indent, horizontal, addition)
      imgui.SetCursorPos(imgui.ImVec2(5,5))

      for i = 1, #arr do
            local button_size = imgui.ImVec2(button_size_x, button_size_y)
            if imgui.MenuOption(arr[i], selected - addition == i, button_size) then
                  selected = i + addition
            end
            imgui.SetCursorPos(imgui.ImVec2(horizontal and (5 + i * button_size_x + (i - 1) * indent) or 5, (not horizontal) and  (5 + i * button_size_y + (i - 1) * indent) or 5))
      end
end

function imgui.CustomButton(name, color, colorHovered, colorActive, size)
      size = size or imgui.ImVec2(0, 0)

      imgui.PushStyleColor(imgui.Col.Button, color)
      imgui.PushStyleColor(imgui.Col.ButtonHovered, colorHovered)
      imgui.PushStyleColor(imgui.Col.ButtonActive, colorActive)

      local result = imgui.Button(name, size)

      imgui.PopStyleColor(3)

      return result
end

function imgui.ApplyTheme()
      imgui.SwitchContext()
      local style = imgui.GetStyle()
      local colors = style.Colors
      local clr = imgui.Col
      local ImVec4 = imgui.ImVec4
      local ImVec2 = imgui.ImVec2

      style.WindowPadding = imgui.ImVec2(8, 8)
      style.WindowRounding = 6
      style.ChildWindowRounding = 5
      style.FramePadding = imgui.ImVec2(5, 3)
      style.FrameRounding = 3.0
      style.ItemSpacing = imgui.ImVec2(5, 4)
      style.ItemInnerSpacing = imgui.ImVec2(4, 4)
      style.IndentSpacing = 21
      style.ScrollbarSize = 10.0
      style.ScrollbarRounding = 13
      style.GrabMinSize = 8
      style.GrabRounding = 1
      style.WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
      style.ButtonTextAlign = imgui.ImVec2(0.5, 0.5)

      colors[clr.Text] = ImVec4(0.95, 0.96, 0.98, 1.00)
      colors[clr.TextDisabled] = ImVec4(0.29, 0.29, 0.29, 1.00)
      colors[clr.WindowBg] = ImVec4(0.14, 0.14, 0.14, 1.00)
      colors[clr.ChildWindowBg] = ImVec4(0.10, 0.10, 0.10, 1.00)
      colors[clr.PopupBg] = ImVec4(0.08, 0.08, 0.08, 0.94)
      colors[clr.Border] = ImVec4(0.14, 0.14, 0.14, 1.00)
      colors[clr.BorderShadow] = ImVec4(1.00, 1.00, 1.00, 0.10)
      colors[clr.FrameBg] = ImVec4(0.22, 0.22, 0.22, 1.00)
      colors[clr.FrameBgHovered] = ImVec4(0.18, 0.18, 0.18, 1.00)
      colors[clr.FrameBgActive] = ImVec4(0.09, 0.12, 0.14, 1.00)
      colors[clr.TitleBg] = ImVec4(0.14, 0.14, 0.14, 0.81)
      colors[clr.TitleBgActive] = ImVec4(1.00, 0.28, 0.28, 1.00)
      colors[clr.TitleBgCollapsed] = ImVec4(0.00, 0.00, 0.00, 0.51)
      colors[clr.MenuBarBg] = ImVec4(0.20, 0.20, 0.20, 1.00)
      colors[clr.ScrollbarBg] = ImVec4(0.02, 0.02, 0.02, 0.39)
      colors[clr.ScrollbarGrab] = ImVec4(0.36, 0.36, 0.36, 1.00)
      colors[clr.ScrollbarGrabHovered] = ImVec4(0.18, 0.22, 0.25, 1.00)
      colors[clr.ScrollbarGrabActive] = ImVec4(0.24, 0.24, 0.24, 1.00)
      colors[clr.ComboBg] = ImVec4(0.24, 0.24, 0.24, 1.00)
      colors[clr.CheckMark] = ImVec4(1.00, 0.28, 0.28, 1.00)
      colors[clr.SliderGrab] = ImVec4(1.00, 0.28, 0.28, 1.00)
      colors[clr.SliderGrabActive]  = ImVec4(1.00, 0.28, 0.28, 1.00)
      colors[clr.Button] = ImVec4(1.00, 0.28, 0.28, 1.00)
      colors[clr.ButtonHovered] = ImVec4(1.00, 0.39, 0.39, 1.00)
      colors[clr.ButtonActive] = ImVec4(1.00, 0.21, 0.21, 1.00)
      colors[clr.Header] = ImVec4(1.00, 0.28, 0.28, 1.00)
      colors[clr.HeaderHovered] = ImVec4(1.00, 0.39, 0.39, 1.00)
      colors[clr.HeaderActive] = ImVec4(1.00, 0.21, 0.21, 1.00)
      colors[clr.ResizeGrip] = ImVec4(1.00, 0.28, 0.28, 1.00)
      colors[clr.ResizeGripHovered] = ImVec4(1.00, 0.39, 0.39, 1.00)
      colors[clr.ResizeGripActive]  = ImVec4(1.00, 0.19, 0.19, 1.00)
      colors[clr.CloseButton] = ImVec4(0.40, 0.39, 0.38, 0.16)
      colors[clr.CloseButtonHovered] = ImVec4(0.40, 0.39, 0.38, 0.39)
      colors[clr.CloseButtonActive] = ImVec4(0.40, 0.39, 0.38, 1.00)
      colors[clr.PlotLines] = ImVec4(0.61, 0.61, 0.61, 1.00)
      colors[clr.PlotLinesHovered] = ImVec4(1.00, 0.43, 0.35, 1.00)
      colors[clr.PlotHistogram] = ImVec4(1.00, 0.21, 0.21, 1.00)
      colors[clr.PlotHistogramHovered] = ImVec4(1.00, 0.18, 0.18, 1.00)
      colors[clr.TextSelectedBg] = ImVec4(1.00, 0.32, 0.32, 1.00)
      colors[clr.ModalWindowDarkening] = ImVec4(0.26, 0.26, 0.26, 0.60)
end

imgui.ApplyTheme()