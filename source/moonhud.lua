script_name('MoonHUD')
script_author('THERION')
script_version(1)
script_description('Multifunctional HUD based on MoonAdditions library')
script_dependencies('MoonAdditions', 'SAMP.lua', 'imGui')

EXPORTS = {
      no_reload = true
}

---- external libraries

inicfg = require("inicfg")
memory = require("memory")
moon_additions = require("MoonAdditions")
samp_events = require("samp.events")
imgui = require("imgui")
encoding = require("encoding")
encoding.default = "CP1251"
utf8 = encoding.UTF8

---- global state

window_state = imgui.ImBool(false)
selected = 1

font_buffer = imgui.ImBuffer(256)

textures = {}

is_galaxy = false

---- modules

fa = require("fontawesome5")
require("const")
require("utils")
require("color")
require("render")
require("widgets")

game = require("game")
server = require("server")

require("menu")

function toggle_kill_list(bool)
      local value = bool and 1 or 0
      return setStructElement(sampGetKillInfoPtr(), 0, 4, value)
end

function toggle_hud(config)
      if not config.GENERAL.on then
            displayHud(true)
            toggle_kill_list(true)
            return
      end

      displayHud(false)
      toggle_kill_list(false)
      set_radar_pos(config.RADAR.pos_x.v, config.RADAR.pos_y.v)
      set_radar_size(config.RADAR.size_x.v, config.RADAR.size_y.v)
end

function on_toggle_configuration_menu()
      window_state.v = not window_state.v
end

function on_every_frame()
      imgui.Process = window_state.v

      if not game.config.GENERAL.on then
            return
      end

      game.on_every_frame()
      server.on_every_frame()
end

function main()
      while not isSampAvailable() do
            wait(0)
      end

      --[[for i = 1, 2069 do
            if sampTextdrawIsExists(i) then
                  local model, rot_x, rot_y, rot_z, zoom = sampTextdrawGetModelRotationZoomVehColor(i)
                  print(i, model, rot_x, rot_y, rot_z, zoom)
            end
      end]]

      game.init()
      server.init()

      toggle_hud(game.config)

      sampRegisterChatCommand("hud", on_toggle_configuration_menu)

      while not sampIsLocalPlayerSpawned() do
            wait(0)
      end

      while true do
            wait(0)
            on_every_frame()
      end
end

function onScriptTerminate(target, _)
      if target ~= script.this then
            return
      end

      game.config.GENERAL.on = false
      toggle_hud(game.config)
end
