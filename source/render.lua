-- no way i would refactor this horribleness

MOON_ADDITIONS_STYLE_OPTIONS = {
      "SUBTITLES",
      "MENU",
      "GOTHIC",
      "PRICEDOWN",
}

MOON_ADDITIONS_ALIGNMENT_OPTIONS = {
      "LEFT",
      "CENTER",
      "RIGHT",
}

function render_rect(left, top, right, bottom, color)
      local a, r, g, b = imfloat4_to_argb(color)
      moon_additions.draw_rect(left, top, right, bottom, r, g, b, a, 0)
end

function render_text(text, left, top, width, height, text_color, shadow_color, style, align, outline)
      local font_align = moon_additions.font_align[MOON_ADDITIONS_ALIGNMENT_OPTIONS[align]]
      local font_style = moon_additions.font_style[MOON_ADDITIONS_STYLE_OPTIONS[style]]
      local text_a, text_r, text_g, text_b = imfloat4_to_argb(text_color)
      local shadow_a, shadow_r, shadow_g, shadow_b = imfloat4_to_argb(shadow_color)
      local wrap_bound = 60000 * (mad_align == moon_additions.font_align.RIGHT and -1 or 1)

      moon_additions.draw_text(text, left, top, font_style, width, height, font_align, wrap_bound, true, false, text_r, text_g, text_b, text_a, outline, 0, shadow_r, shadow_g, shadow_b, shadow_a, false)
end

function render_texture(texture, left, top, right, bottom, color)
      local a, r, g, b = imfloat4_to_argb(color)

      texture:draw(left, top, right, bottom, r, g, b, a, 0)
end

function render_simple_texture(options, texture)
      local left, top, right, bottom, color
      left = options.icon_pos_x.v
      top = options.icon_pos_y.v
      right = options.icon_pos_x.v + options.icon_size.v
      bottom = options.icon_pos_y.v + options.icon_size.v
      color = options.icon_color.v

      render_texture(texture, left, top, right, bottom, color)
end

function render_simple_borderbox(options, fill)
      local left, top, right, bottom, color

      left = options.bar_pos_x.v
      top = options.bar_pos_y.v
      right = options.bar_pos_x.v + options.bar_size_x.v
      bottom = options.bar_pos_y.v + options.border_size.v
      color = options.border_color.v
      render_rect(left, top, right, bottom, color)

      left = options.bar_pos_x.v
      top = options.bar_pos_y.v + options.bar_size_y.v - options.border_size.v
      right = options.bar_pos_x.v + options.bar_size_x.v
      bottom = options.bar_pos_y.v + options.bar_size_y.v
      color = options.border_color.v
      render_rect(left, top, right, bottom, color)

      left = options.bar_pos_x.v
      top = options.bar_pos_y.v + options.border_size.v
      right = options.bar_pos_x.v + options.border_size.v
      bottom = options.bar_pos_y.v + options.bar_size_y.v - options.border_size.v
      color = options.border_color.v
      render_rect(left, top, right, bottom, color)

      left = options.bar_pos_x.v + options.bar_size_x.v - options.border_size.v
      top = options.bar_pos_y.v + options.border_size.v
      right = options.bar_pos_x.v + options.bar_size_x.v
      bottom = options.bar_pos_y.v + options.bar_size_y.v - options.border_size.v
      color = options.border_color.v
      render_rect(left, top, right, bottom, color)

      left = options.bar_pos_x.v + options.border_size.v
      top = options.bar_pos_y.v + options.border_size.v
      right = options.bar_pos_x.v - options.border_size.v + options.bar_size_x.v
      bottom = options.bar_pos_y.v + options.bar_size_y.v - options.border_size.v
      color = options.background_color.v
      render_rect(left, top, right, bottom, color)

      if fill > 0 then
            left = options.bar_pos_x.v + options.border_size.v
            top = options.bar_pos_y.v + options.border_size.v
            right = options.bar_pos_x.v - options.border_size.v + options.bar_size_x.v * fill
            bottom = options.bar_pos_y.v + options.bar_size_y.v - options.border_size.v
            color = options.main_color.v
            render_rect(left, top, right, bottom, color)
      end
end

function render_simple_textdraw(options, value)
      local text, left, top, width, height
      local text_color, shadow_color, style, align, outline

      text = value
      left = options.pos_x.v
      top = options.pos_y.v
      width = options.scale_x.v
      height = options.scale_y.v
      text_color = options.text_color.v
      shadow_color = options.shadow_color.v
      style = options.style.v
      align = options.align.v
      outline = 1
      -- no outline config param exists now so rendering as 1

      render_text(text, left, top, width, height, text_color, shadow_color, style, align, outline)
end