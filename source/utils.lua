local SCRIPT_MESSAGE_FORMAT = "{FB4343}[MoonHUD]{FFFFFF}: %s"

function add_chat_message(msg)
      local msg_formatted = SCRIPT_MESSAGE_FORMAT:format(msg)
      sampAddChatMessage(msg_formatted, -1)
end

function clamp(var, min, max)
      if var < min then
            var = min
      end
      if var > max then
            var = max
      end

      return var
end

function hide_textdraw(id)
      sampTextdrawSetPos(id, 40000, 40000)
end